import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AccountPage extends StatefulWidget {
  @override
  State<AccountPage> createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
  final myNameController = TextEditingController();
  final myEmailController = TextEditingController();
  bool _obscureText = true;

  var orangeColor = const Color(0xffF0F3BD);
  var btnColor = const Color(0xff333333);
  ProgressDialog pr;
  var futureAccount = false;

  @override
  void initState() {
    super.initState();
    getAccountData().then((val){
      var jsonData = json.decode(val);
      myNameController.text = jsonData["name"];
      myEmailController.text = jsonData["email"];
      setState(() {
        futureAccount = true;
      });
    });
  }
  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: const Color(0xFF05668D),
      appBar: PreferredSize(
          child: Container(
            padding: EdgeInsets.only(top: 32),
            color: const Color(0xff131313),
            child: Column(
              children: <Widget>[
                Center(
                  child: Text(
                    "PETiD®",
                    style: TextStyle(
                        fontFamily: "Squada1",
                        color: orangeColor, fontSize: 30, letterSpacing: 1),
                    textAlign: TextAlign.justify,
                  ),
                ),
                Center(
                  child: Text(
                    "INTELLIGENT PET IDENTIFICATION",
                    style: TextStyle(
                        fontFamily: "Squada1",
                        color: const Color(0xffffffff),
                        fontSize: 7,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1),
                    textAlign: TextAlign.justify,
                  ),
                ),
              ],
            ),
          ),
          preferredSize: Size(360, 45)),
      body: Container(
        padding: EdgeInsets.only(top: 16),
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          gradient: LinearGradient(colors: [
            const Color(0xFF028090),
            const Color(0xFF05668D),
          ], begin: Alignment.bottomCenter, end: Alignment.centerRight),
        ),
        child: Center(
          child: futureAccount?Container(
            padding: EdgeInsets.only(top: 8),
            width: 333,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "My Account Info:",
                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      color: orangeColor),
                ),
                TextField(
                  controller: myNameController,
                  style: new TextStyle(color: Colors.white),
                  decoration: InputDecoration(
                    hintText: "Name",
                    hintStyle: TextStyle(color: Colors.white),
                    enabledBorder: new UnderlineInputBorder(
                        borderSide: new BorderSide(color: Colors.white)),
                  ),
                ),
                TextField(
                  controller: myEmailController,
                  style: new TextStyle(color: Colors.white),
                  decoration: InputDecoration(
                    hintText: "Email",
                    hintStyle: TextStyle(color: Colors.white),
                    enabledBorder: new UnderlineInputBorder(
                        borderSide: new BorderSide(color: Colors.white)),
                  ),
                ),
                TextField(
                  keyboardType: TextInputType.visiblePassword,
                  textInputAction: TextInputAction.next,
                  style: new TextStyle(color: Colors.white),
                  decoration: InputDecoration(
                    suffixIcon: IconButton(
                        icon: _obscureText
                            ? Icon(
                          Icons.visibility,
                          color: Colors.white,
                        )
                            : Icon(
                          Icons.visibility_off,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          _toggle();
                        }),
                    hintText: "Password",
                    hintStyle: TextStyle(color: Colors.white),
                    enabledBorder: new UnderlineInputBorder(
                        borderSide: new BorderSide(color: Colors.white)),
                  ),
                ),
                TextField(
                  keyboardType: TextInputType.visiblePassword,
                  style: new TextStyle(color: Colors.white),
                  decoration: InputDecoration(
                    suffixIcon: IconButton(
                        icon: _obscureText
                            ? Icon(
                          Icons.visibility,
                          color: Colors.white,
                        )
                            : Icon(
                          Icons.visibility_off,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          _toggle();
                        }),
                    hintText: "Confirm Password",
                    hintStyle: TextStyle(color: Colors.white),
                    enabledBorder: new UnderlineInputBorder(
                        borderSide: new BorderSide(color: Colors.white)),
                  ),
                ),
                Padding(padding: EdgeInsets.all(8)),
                /*Container(
                  height: 45,
                  width: 333,
                  margin: EdgeInsets.only(top: 8, bottom: 8),
                  decoration: BoxDecoration(
                      border: Border.all(color: orangeColor)),
                  child: Theme(
                      data: Theme.of(context).copyWith(
                        canvasColor: Colors.grey[900],
                      ),
                      child: Align(
                          alignment: AlignmentDirectional.center,
                          child: DropdownButton(
                              isDense: true,
                              iconEnabledColor: Colors.white,
                              underline:
                              Container(color: Colors.transparent),
                              hint: Container(
                                  child: Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.center,
                                    crossAxisAlignment:
                                    CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Image.network(
                                        "https://hotemoji.com/images/dl/0/american-flag-emoji-by-google.png",
                                        height: 32,
                                      ),
                                      Padding(padding: EdgeInsets.all(8)),
                                      Text(
                                        "Choose Language",
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ],
                                  )),
                              elevation: 16,
                              style: TextStyle(color: Colors.black)))),
                ),*/
                InkWell(
                  onTap: (){
                    pr.style(message: "Updating, Please wait..");
                    pr.show();
                    updateAccount().then((val){
                      pr.hide().then((onval){
                        var jsonData = json.decode(val);
                        if(!jsonData["error"]){
                          Fluttertoast.showToast(msg: jsonData["message"]);
                        }
                      });
                    });
                  },
                  child: Container(
                    width: 333,
                    height: 45,
                    margin: EdgeInsets.symmetric(vertical: 8),
                    child: Center(
                        child: Text(
                          "Save",
                          style: TextStyle(
                              fontFamily: "Squada1",
                              fontWeight: FontWeight.w500, fontSize: 18),
                        )),
                    color: orangeColor,
                  ),
                ),
                /*Container(
                  width: 333,
                  height: 45,
                  margin: EdgeInsets.symmetric(vertical: 8),
                  child: Center(
                      child: Text(
                        "Manage Subscription",
                        style: TextStyle(
                            fontFamily: "Squada1",
                            fontWeight: FontWeight.w500,
                            fontSize: 18,
                            color: Colors.white),
                      )),
                  color: btnColor,
                ),
                Container(
                  width: 333,
                  height: 45,
                  margin: EdgeInsets.symmetric(vertical: 8),
                  child: Center(
                      child: Text(
                        "Rate PETiD in the App Store",
                        style: TextStyle(
                            fontFamily: "Squada1",
                            fontWeight: FontWeight.w500,
                            fontSize: 18,
                            color: Colors.white),
                      )),
                  color: btnColor,
                ),
                Container(
                  width: 333,
                  height: 45,
                  margin: EdgeInsets.symmetric(vertical: 8),
                  child: Center(
                      child: Text(
                        "Get Support",
                        style: TextStyle(
                            fontFamily: "Squada1",
                            fontWeight: FontWeight.w500,
                            fontSize: 18,
                            color: Colors.white),
                      )),
                  color: btnColor,
                )*/
              ],
            ),
          ):CircularProgressIndicator()
        ),
      ),
    );
  }

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }


  Future<String> updateAccount() async {
    final prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString("access_token");
    Map data = {
      'name': myNameController.text,
      'email': myEmailController.text,
    };
    var bodyJson = json.encode(data);
    String url = 'https://pet-id.app/deploy/public/api/my-account';
    var response = await http.post(url,
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
          "Authorization": "Bearer " + accessToken,
        },
        body: bodyJson);
    return response.body;
  }
}


Future<String> getAccountData() async {
  final prefs = await SharedPreferences.getInstance();
  String accessToken = prefs.getString("access_token");

  String url = 'https://pet-id.app/deploy/public/api/my-account';
  var response = await http.get(
    url,
    headers: {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer " + accessToken,
    },
  );
  return response.body;
}
