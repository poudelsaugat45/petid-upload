import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AddPetPage extends StatefulWidget {
  String petId;

  AddPetPage({this.petId});

  @override
  _AddPetPageState createState() => _AddPetPageState(petId: petId);
}

class _AddPetPageState extends State<AddPetPage> {
  var petList = [];
  String petId;
  ProgressDialog pr;

  _AddPetPageState({this.petId});

  final myNameController = TextEditingController();
  final colorController = TextEditingController();
  final breedController = TextEditingController();
  final notesController = TextEditingController();
  var imageOne;
  var imageTwo;
  String _image1;
  String _image2;
  String gender = "Male";
  int status = 0;

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context);
    var orangeColor = const Color(0xffF0F3BD);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: PreferredSize(
          child: Container(
            padding: EdgeInsets.only(top: 32),
            color: const Color(0xff131313),
            child: Column(
              children: <Widget>[
                Center(
                  child: Text(
                    "PETiD®",
                    style: TextStyle(
                        fontFamily: "Squada1",
                        color: orangeColor, fontSize: 30, letterSpacing: 1),
                    textAlign: TextAlign.justify,
                  ),
                ),
                Center(
                  child: Text(
                    "INTELLIGENT PET IDENTIFICATION",
                    style: TextStyle(
                        fontFamily: "Squada1",
                        color: const Color(0xffffffff),
                        fontSize: 7,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1),
                    textAlign: TextAlign.justify,
                  ),
                ),
              ],
            ),
          ),
          preferredSize: Size(360, 45)),
      body: Container(
          padding: EdgeInsets.only(top: 16),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            gradient: LinearGradient(colors: [
              const Color(0xFF028090),
              const Color(0xFF05668D),
            ], begin: Alignment.bottomCenter, end: Alignment.centerRight),
          ),
          child: Center(
            child: Container(
              width: 328,
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      InkWell(
                        onTap: () {
                          getImage().then((file) {
                            getbaseImage(file).then((baseimage) {
                              setState(() {
                                _image1 = baseimage;
                              });
                            });
                          });
                        },
                        child: Container(
                            width: 149,
                            height: 124,
                            child: _image1 == null
                                ? Image.asset("assets/take_photo.png",
                                    fit: BoxFit.fitHeight)
                                : Image.memory(base64Decode(_image1))),
                      ),
                      InkWell(
                        onTap: () {
                          getImage().then((file) {
                            getbaseImage(file).then((baseimage) {
                              setState(() {
                                _image2 = baseimage;
                              });
                            });
                          });
                        },
                        child: Container(
                            width: 149,
                            height: 124,
                            child: _image2 == null
                                ? Image.asset("assets/take_photo.png",
                                    fit: BoxFit.fitHeight)
                                : Image.memory(base64Decode(_image2))),
                      )
                    ],
                  ),
                  TextField(
                    controller: myNameController,
                    style: new TextStyle(color: Colors.white),
                    maxLength: 12,
                    decoration: InputDecoration(
                      suffixIcon: Icon(
                        Icons.check,
                        color: orangeColor,
                        size: 24,
                      ),
                      labelText: "Name:",
                      hintText: "Name",
                      alignLabelWithHint: true,
                      hintStyle:
                          TextStyle(color: Color(0x44ffffff), fontSize: 15),
                      labelStyle: TextStyle(color: Colors.white),
                      enabledBorder: new UnderlineInputBorder(
                          borderSide: new BorderSide(color: Colors.white)),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 24.0, bottom: 8),
                    child: Row(
                      children: <Widget>[
                        Text("Sex:",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 15,
                                fontWeight: FontWeight.bold)),
                        Padding(
                            padding: EdgeInsets.symmetric(
                                vertical: 8, horizontal: 8)),
                        GestureDetector(
                          child: Padding(
                            padding: const EdgeInsets.only(right: 8.0),
                            child: Text(
                              "Male",
                              style: TextStyle(
                                  color: gender == "Male"
                                      ? orangeColor
                                      : Colors.white,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          onTap: () {
                            setState(() {
                              gender = "Male";
                            });
                          },
                        ),
                        GestureDetector(
                          child: Padding(
                            padding: const EdgeInsets.only(right: 8.0),
                            child: Text(
                              "Female",
                              style: TextStyle(
                                  color: gender == "Female"
                                      ? orangeColor
                                      : Colors.white,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          onTap: () {
                            setState(() {
                              gender = "Female";
                            });
                          },
                        ),
                      ],
                    ),
                  ),
                  TextField(
                    controller: colorController,
                    style: new TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                      labelText: "Color: ",
                      suffixIcon: Icon(
                        Icons.check,
                        color: orangeColor,
                        size: 24,
                      ),
                      hintText: "Color",
                      hintStyle:
                          TextStyle(color: Color(0x44ffffff), fontSize: 15),
                      labelStyle: TextStyle(color: Colors.white),
                      enabledBorder: new UnderlineInputBorder(
                          borderSide: new BorderSide(color: Colors.white)),
                    ),
                  ),
                  TextField(
                    controller: breedController,
                    style: new TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                      labelText: "Breed: ",
                      hintText: "Breed",
                      suffixIcon: Icon(
                        Icons.check,
                        color: orangeColor,
                        size: 24,
                      ),
                      hintStyle:
                          TextStyle(color: Color(0x44ffffff), fontSize: 15),
                      labelStyle: TextStyle(color: Colors.white),
                      enabledBorder: new UnderlineInputBorder(
                          borderSide: new BorderSide(color: Colors.white)),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: Row(
                      children: <Widget>[
                        Text("Status:",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 15,
                                fontWeight: FontWeight.bold)),
                        Padding(
                            padding: EdgeInsets.symmetric(
                                vertical: 8, horizontal: 8)),
                        GestureDetector(
                          child: Padding(
                            padding: const EdgeInsets.only(right: 8.0),
                            child: Text(
                              "Lost",
                              style: TextStyle(
                                  color: status == 0
                                      ? Color(0xFF02C39A)
                                      : Colors.white,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          onTap: () {
                            setState(() {
                              status = 0;
                            });
                          },
                        ),
                        GestureDetector(
                          child: Padding(
                            padding: const EdgeInsets.only(right: 8.0),
                            child: Text(
                              "Safe",
                              style: TextStyle(
                                  color:
                                      status == 1 ? orangeColor : Colors.white,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          onTap: () {
                            setState(() {
                              status = 1;
                            });
                          },
                        ),
                      ],
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(top: 8)),
                  Container(
                    color: Color(0x55333333),
                    child: TextField(
                      textInputAction: TextInputAction.done,
                      controller: notesController,
                      style: new TextStyle(color: Colors.white),
                      maxLines: 5,
                      minLines: 5,
                      decoration: InputDecoration(
                        isDense: false,
                        fillColor: Color(0x55333333),
                        labelText: "Notes: ",
                        hintText: "Notes",
                        hintMaxLines: 1,
                        alignLabelWithHint: true,
                        hintStyle:
                            TextStyle(color: Color(0x44ffffff), fontSize: 15),
                        labelStyle: TextStyle(color: Colors.white),
                        enabledBorder: new OutlineInputBorder(
                            borderSide:
                                new BorderSide(color: Color(0xFFF0F3BD))),
                        focusedBorder: new OutlineInputBorder(
                            borderSide:
                                new BorderSide(color: Color(0xFFF0F3BD))),
                      ),
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(top: 16)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      InkWell(
                        onTap: () {
                          if (myNameController.text == null ||
                              myNameController.text.trim() == "") {
                            Fluttertoast.showToast(msg: "Name cannot be empty");
                          } else {
                            if (_image1 == null || _image2 == null) {
                              Fluttertoast.showToast(
                                  msg: "Both images are necessary");
                            } else {
                              pr.show();
                              insertPetData().then((resp) {
                                pr.hide().then((v) {
                                  try {
                                    var responseJson = json.decode(resp);
                                    Fluttertoast.showToast(
                                        msg: responseJson["message"]);
                                    if (responseJson["status"]) {
                                      Navigator.pop(context);
                                    }
                                  } catch (e) {}
                                });
                              });
                            }
                          }
                        },
                        child: Container(
                          width: 150,
                          height: 45,
                          color: orangeColor,
                          child: Center(
                            child: Text(
                              "Add",
                              style:
                                  TextStyle(
                                      fontFamily: "Squada1",color: Colors.black, fontSize: 18),
                            ),
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: (){
                          Navigator.pop(context);
                        },
                        child: Container(
                          width: 150,
                          height: 45,
                          color: Color(0xFF333333),
                          child: Center(
                            child: Text(
                              "Cancel",
                              style:
                                  TextStyle(
                                      fontFamily: "Squada1",color: Colors.white70, fontSize: 18),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  /*GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => OrderTagPage()),
                      );
                    },
                    child: Container(
                      margin: EdgeInsets.only(top: 16),
                      height: 45,
                      color: orangeColor,
                      child: Center(
                        child: Text(
                          "Order Tag For Rocco",
                          style: TextStyle(color: Colors.black, fontSize: 18),
                        ),
                      ),
                    ),
                  ),*/
                ],
              ),
            ),
          )),
    );
  }

  Future<File> getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    return image;
  }

  Future<String> insertPetData() async {
    final prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString("access_token");
    Map data = {
      'name': myNameController.text,
      'gender': gender,
      'color': colorController.text,
      'breed': breedController.text,
      'message': notesController.text,
      'image1': _image1!=null?"data:image/jpeg;base64," + _image1:null,
      'image2': _image2!=null?"data:image/jpeg;base64," + _image2:null,
      'status': "$status",
    };
    print(data);
    var bodyJson = json.encode(data);
    String url = 'https://pet-id.app/deploy/public/api/my-pet';
    var response = await http.post(url,
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
          "Authorization": "Bearer " + accessToken,
        },
        body: bodyJson);
    return response.body;
  }
}

Future<String> getbaseImage(File image) async {
  List<int> imageBytes = await image.readAsBytesSync();
  String base64Image = base64Encode(imageBytes);
  return base64Image;
}
