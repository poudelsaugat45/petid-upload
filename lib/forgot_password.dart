import 'dart:convert';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:pet_id/signup_page.dart';
import 'package:progress_dialog/progress_dialog.dart';

class ForgotPasswordPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => ForgotPasswordPageState();
}

class ForgotPasswordPageState extends State<ForgotPasswordPage> {
  double height;
  double width;
  bool _obscureText = true;
  var orangeColor = const Color(0xffF0F3BD);
  var textColor = const Color(0xffF2F2F2);
  ProgressDialog pr;

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  final myFirstNameController = TextEditingController();
  final myLastNameController = TextEditingController();
  final myPhoneController = TextEditingController();
  final myUserNameController = TextEditingController();
  final myPasswordController = TextEditingController();
  final myPasswordAgainController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  var subscribed = false;

  var firstName;
  var email;
  var lastName;

  @override
  void initState() {
    super.initState();
    asyncInitState();
  }

  void asyncInitState() async {}

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context);
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(colors: [
              const Color(0xFF028090),
              const Color(0xFF05668D),
            ], begin: Alignment.bottomCenter, end: Alignment.centerRight),
          ),
          child: Column(
            children: <Widget>[
              Container(
                child: Column(
                  children: <Widget>[
                    Container(
                      height: height / 12,
                    ),
                    Container(
                      child: Column(
                        children: <Widget>[
                          Text(
                            "PETiD®",
                            style: TextStyle(
                                fontFamily: "Squada1",
                                fontSize: 70,
                                color: orangeColor),
                          ),
                          Text(
                            "INTELLIGENT PET IDENTIFICATION",
                            style: TextStyle(
                                fontSize: 12,
                                fontFamily: "Squada1",
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                                letterSpacing: 0.1),
                          )
                        ],
                      ),
                    ),
                    Container(
                      width: 258,
                      margin: EdgeInsets.only(
                          top: height / 30, bottom: height / 16, right: 0),
                      child: Align(
                        alignment: AlignmentDirectional.centerStart,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Password reset, \nEnter your email address:",
                              style: TextStyle(
                                  color: textColor,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.left,
                            ),
                            Padding(padding: EdgeInsets.all(8)),
                            TextField(
                              controller: myFirstNameController,
                              style: new TextStyle(color: Colors.white),
                              decoration: InputDecoration(
                                hintText: "Email",
                                hintStyle: TextStyle(
                                    color: textColor,
                                    fontWeight: FontWeight.bold),
                                enabledBorder: new UnderlineInputBorder(
                                    borderSide:
                                        new BorderSide(color: Colors.white)),
                              ),
                            ),
                            Padding(padding: EdgeInsets.all(16)),
                            GestureDetector(
                              child: Container(
                                  width: 258,
                                  height: 45,
                                  color: orangeColor,
                                  child: Center(
                                      child: Text(
                                    "Reset Password",
                                    style: TextStyle(
                                        fontSize: 17,
                                        fontWeight: FontWeight.w400),
                                  ))),
                              onTap: () {
                                pr.show();
                                resetPassword(myFirstNameController.text).then((value){
                                  pr.hide().then((val){
                                    var valueJson = json.decode(value);
                                    Fluttertoast.showToast(msg: valueJson["message"]);
                                    if(!valueJson["error"]){
                                    }
                                  });
                                });
                              },
                            ),
                            Padding(padding: EdgeInsets.all(8)),
                            Container(
                              alignment: AlignmentDirectional.center,
                              child: RichText(
                                textAlign: TextAlign.center,
                                text: TextSpan(
                                    text: 'New user? ',
                                    style: TextStyle(
                                        fontFamily: "Squada",
                                        fontSize: 15,
                                        color: textColor,
                                        fontWeight: FontWeight.bold),
                                    children: [
                                      TextSpan(
                                          text: "Signup",
                                          style: TextStyle(fontSize: 14, color: orangeColor),
                                          recognizer: TapGestureRecognizer()
                                            ..onTap = () {
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        SignupPage()),
                                              );
                                            }),
                                    ]),
                              ),
                            ),
                            Container(
                              width: 328,
                              margin: EdgeInsets.only(top: 12),
                              child: RichText(
                                textAlign: TextAlign.center,
                                text: TextSpan(
                                    style: TextStyle(
                                      fontFamily: "Squada",
                                      fontSize: 13,
                                      color: textColor,
                                    ),
                                    text: "By using PET-ID you agree to our ",
                                    children: [
                                      TextSpan(
                                        recognizer: new TapGestureRecognizer()
                                          ..onTap = () {
                                            Navigator.pushNamed(
                                                context, '/terms');
                                          },
                                        text: "Terms Of Service",
                                        style: TextStyle(
                                            decoration:
                                                TextDecoration.underline,
                                            color: orangeColor),
                                      ),
                                      TextSpan(
                                        text:
                                            ". Learn how we process your data by viewing our ",
                                      ),
                                      TextSpan(
                                          text: "Privacy Policy",
                                          style: TextStyle(
                                              decoration:
                                                  TextDecoration.underline,
                                              color: orangeColor),
                                          recognizer: new TapGestureRecognizer()
                                            ..onTap = () {
                                              Navigator.pushNamed(
                                                context,
                                                '/privacy',
                                              );
                                            }),
                                    ]),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )),
    );
  }

  @override
  void dispose() {
    myUserNameController.dispose();
    myPasswordController.dispose();
    myPasswordAgainController.dispose();
    super.dispose();
  }

  Future<String> resetPassword(String email) async {
    String url = 'https://pet-id.app/deploy/public/api/auth/password/email';
    Map data = {'email': '$email'};
    var bodyJson = json.encode(data);
    var response = await http.post(url,
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          "Accept": "application/x-www-form-urlencoded"
        },
        body: bodyJson);
    print(response.body);
    return response.body;
  }
}
