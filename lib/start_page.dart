import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:pet_id/login_page.dart';
import 'package:pet_id/signup_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'home_page.dart';

class StartPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => StartPageState();
}

class StartPageState extends State<StartPage> {
  int pageIndex = 0;
  double height;
  double width;
  String url1 = "https://images.pexels.com/photos/1108099/pexels-photo-1108099.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500";
  String url2 = "assets/img/image1.png";
  Map<String, String> languageMap = {
    "Catalan(Catalan)": "ca_ES",
    "Chinese(Simplified)": "zh_CN",
    "Chinese(Traditional)": "zh_HK",
    "Croatian(Croatia)": "hr_HR",
    "Czech(Czech)": "cs_CZ",
    "Danish(Denmark)": "da_DK",
    "Dutch(Netherlands)": "nl_NL",
    "English(US)": "en_US",
    "English(UK)": "en_UK",
    "English(Australia)": "en_AU",
    "English(Canada)": "en_CA",
    "Finnish(Finland)": "fi_FI",
    "French(Canada)": "fr_CA",
    "French(France)": "fr_FR",
    "Greek(Greece)": "el_GR",
    "German(Germany)": "de_DE",
    "Hindi(India)": "hi_IN",
    "Hungarian(Hungary)": "hu_HU",
    "Indonesian(Indonesia)": "id_ID",
    "Italian(Italy)": "it_IT",
    "Japanese(Japan)": "ja_JP",
    "Korean(Korea)": "ko_KR",
    "Macedonian(Macedonia)": "mk_MK",
    "Malay(Malaysia)": "ms_MY",
    "Norwegian(Norway)": "nb_NO",
    "Polish(Poland)": "pl_PL",
    "Portuguese(Portugal)": "pt_PT",
    "Romanian(Romania)": "ro_RO",
    "Russian(Russia)": "ru_RU",
    "Slovak(Slovakia)": "sk_SK",
    "Spanish(Mexico)": "es_MX",
    "Spanish(Spain)": "es_SP",
    "Swedish(Sweden)": "sv_SE",
    "Thai(Thailand)": "th_TH",
    "Turkish(Turkey)": "tr_TR",
    "Ukrainian(Ukrain)": "uk_UA",
    "Vietnamese(Vietnam)": "vi_VN",
  };
  var selectedItem;

  @override
  void initState() {
    super.initState();
    _checkLogin(context).then((val) {
      if (val) {
        Navigator.pushReplacementNamed(context, '/mainhome');
      } else {
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery
        .of(context)
        .size
        .width;
    height = MediaQuery
        .of(context)
        .size
        .height;
    var _pageController = PageController(
      initialPage: pageIndex
    );

    var orangeColor = const Color(0xffF0F3BD);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [
                const Color(0xFF05668D),
                const Color(0xFF028090),
              ],
              begin: Alignment.bottomCenter,
              end: Alignment.centerRight),
        ),
        child: Stack(
          children: [
            Align(
              alignment: AlignmentDirectional.bottomCenter,
              child: Container(
                height: MediaQuery.of(context).size.height/1.7,
                decoration: BoxDecoration(
                    image: DecorationImage(image: AssetImage("assets/homebg2.png",), fit: BoxFit.fitHeight)
                ),
              ),
            ),

            Container(

                child: Column(
                  children: <Widget>[
                    Container(
                      height: MediaQuery
                          .of(context)
                          .size
                          .height / 2.2,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage("assets/home-bg.png"), fit: BoxFit.fill)),
                      child: Column(
                        children: <Widget>[
                          Container(
                            height: height / 12,
                          ),
                          Container(
                            child: Column(
                              children: <Widget>[
                                Text(
                                  "PETiD®",
                                  style: TextStyle(
                                      fontFamily: "Squada1",
                                      fontSize: 70,
                                      color: orangeColor),
                                ),
                                Text(
                                  "INTELLIGENT PET IDENTIFICATION",
                                  style: TextStyle(
                                      fontFamily: "Squada1",
                                      fontSize: 12,
                                      color: Colors.white,
                                      fontWeight: FontWeight.w700,
                                      letterSpacing: 0.1),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: 200,
                      child: PageView(
                        controller: _pageController,
                        onPageChanged: (page) {
                          setState(() {
                            pageIndex = page;
                          });
                        },
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(
                                  left: width / 14,
                                  right: width / 14,
                                  top: height / 13,),
                                child: Column(
                                  children: <Widget>[
                                    RichText(
                                      textAlign: TextAlign.center,
                                      text: TextSpan(
                                          style: TextStyle(
                                            fontFamily: "Squada",

                                          ),
                                          children: [
                                            TextSpan(
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.bold,
                                                  wordSpacing: 1),
                                              text:
                                              'Over ',
                                            ),
                                            TextSpan(
                                              style: TextStyle(
                                                  color: orangeColor,
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.bold,
                                                  wordSpacing: 1),
                                              text:
                                              '10 million ',
                                            ),
                                            TextSpan(
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 14,
                                                    wordSpacing: 1),
                                                text: "pets were lost last year.  \n- American Humane Society \n\n"
                                            ),
                                            TextSpan(
                                              style: TextStyle(
                                                  fontFamily: "Squada",
                                                  color: orangeColor,
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.bold,
                                                  wordSpacing: 1),
                                              text:
                                              'PETiD ',
                                            ),
                                            TextSpan(
                                                style: TextStyle(
                                                    fontFamily: "Squada",
                                                    color: Colors.white,
                                                    fontSize: 14,
                                                    wordSpacing: 1),
                                                text:
                                                'is a service that makes it easy for people to return lost pets to their owners. We do this by creating custom tags for your pets that embeds your recovery information.'),

                                          ]),
                                    ),

                                  ],
                                ),
                              ),
                            ],
                          ),
                          Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                GestureDetector(
                                  onTap: () {},
                                  child: Container(
                                    height: 52,
                                    width: 296,
                                    //color: orangeColor,
                                    child: Center(child: Text(
                                      "GET YOUR PET A SMART TAG",
                                      style: TextStyle(
                                        color: Colors.white,
                                          fontFamily: "Squada1",
                                          fontSize: 18),)),
                                  ),
                                ),
                                Padding(padding: EdgeInsets.all(8)),
                                /*Container(
                                  height: 52,
                                  width: 296,
                                  decoration:
                                  BoxDecoration(
                                      border: Border.all(color: orangeColor)),
                                  child: Theme(
                                    data: Theme.of(context).copyWith(
                                      canvasColor: Colors.grey[900],
                                    ),
                                    child: Align(
                                      alignment: AlignmentDirectional.center,
                                      child: DropdownButton(
                                          isDense: true,
                                          iconEnabledColor: Colors.white,
                                          underline: Container(
                                              color: Colors.transparent),
                                          hint: Container(
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment
                                                    .center,
                                                crossAxisAlignment: CrossAxisAlignment
                                                    .center,
                                                children: <Widget>[
                                                  Image.network(
                                                    "https://hotemoji.com/images/dl/0/american-flag-emoji-by-google.png",
                                                    height: 32,
                                                  ),
                                                  Padding(
                                                      padding: EdgeInsets.all(8)),
                                                  Text(
                                                    "Choose Language",
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  ),
                                                ],
                                              )),
                                          elevation: 14,
                                          value: selectedItem,
                                          style: TextStyle(color: Colors.black),
                                          items: languageMap
                                              .map((key, value) {
                                            var ccode = value.split("_")[1]
                                                .toLowerCase();
                                            if (ccode == "uk") {
                                              ccode = "gb";
                                            }
                                            if (ccode == "sp") {
                                              ccode = "gb";
                                            }
                                            return MapEntry(
                                                key,
                                                DropdownMenuItem<String>(
                                                  value: key,
                                                  child: Container(
                                                      height: 48,
                                                      child: Row(
                                                        children: <Widget>[
                                                          Padding(
                                                              padding: EdgeInsets
                                                                  .all(4)),
                                                          Text(
                                                            key,
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white),
                                                          ),
                                                        ],
                                                      )),
                                                ));
                                          })
                                              .values
                                              .toList(),
                                          onChanged: (val) {
                                            setState(() {
                                              selectedItem = val;
                                            });
                                            *//*SystemChannels.platform
                                        .invokeMethod('SystemNavigator.pop');*//*
                                          }),
                                    ),
                                  ),
                                ),*/
                              ],
                            ),
                          ),

                          ],
                      ),
                    ),

                    Container(
                      width: 296,
                      margin: EdgeInsets.only(
                        top: height / 32,
                        bottom: height / 82,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          GestureDetector(
                            onTap: () {
                              Navigator.pushReplacementNamed(
                                context,
                                "/login",
                              );                             },
                            child: Text(
                              "LOGIN",
                              style: TextStyle(
                                  fontFamily: "Squada1",
                                  color: Colors.white,
                                  fontSize: 30,
                                  letterSpacing: 1,
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.pushReplacementNamed(
                                context,
                                "/signup",
                              );                            },
                            child: Text("SIGNUP",
                              style: TextStyle(
                                  fontFamily: "Squada1",
                                  color: orangeColor,
                                  fontSize: 30,
                                  letterSpacing: 1,
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          GestureDetector(
                            child: Container(
                              width: 32,
                              height: 20,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color:
                                  pageIndex == 0 ? orangeColor : Color(0xFF4f4f4f)),
                            ),
                            onTap: () {
                              if (pageIndex == 1) {
                                setState(() {
                                  pageIndex = 0;
                                });
                                _pageController.animateToPage(0, duration: Duration(milliseconds: 500), curve: Curves.easeIn);
                              }
                            },
                          ),
                          GestureDetector(
                              child: Container(
                                width: 32,
                                height: 20,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: pageIndex == 1
                                        ? orangeColor
                                        : Color(0xFF4f4f4f)),
                              ),
                              onTap: () {
                                if (pageIndex == 0) {
                                  setState(() {
                                    pageIndex = 1;
                                    _pageController.animateToPage(1, duration: Duration(milliseconds: 500), curve: Curves.easeOut);
                                  });
                                }
                              })
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          left: width / 20, right: width / 20, top: height / 40),
                      child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                            style: TextStyle(
                              fontFamily: "Squada",
                              fontSize: 13,
                              color: Colors.white,
                            ),
                            text: "By using PET-ID you agree to our ",
                            children: [
                              TextSpan(
                                recognizer: new TapGestureRecognizer()
                                  ..onTap = () {
                                    Navigator.pushNamed(context, '/terms');
                                  },
                                text: "Terms Of Service",
                                style:
                                TextStyle(decoration: TextDecoration.underline, color: orangeColor),
                              ),
                              TextSpan(
                                text: ". Learn how we process your data by viewing our ",
                              ),
                              TextSpan(
                                  text: "Privacy Policy",
                                  style: TextStyle(
                                    decoration: TextDecoration.underline,
                                      color: orangeColor
                                  ),
                                  recognizer: new TapGestureRecognizer()
                                    ..onTap = () {
                                      Navigator.pushNamed(context, '/privacy');
                                    }),
                            ]),
                      ),
                    ),
                  ],
                )),

            Align(
              child: Container(
                height: 254,
                width: 274,
                padding: const EdgeInsets.all(32.0),
                margin: EdgeInsets.only(bottom: 250),
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage("assets/ptagsh.png"))
                ),
              ),
              alignment: AlignmentDirectional.center,
            )
            ,
          ],
        ),
      ),
    );
  }

  Future<bool> _checkLogin(BuildContext context) async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.get("access_token") != null) {
      return true;
    } else
      return false;
  }
}
