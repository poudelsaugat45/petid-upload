import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:pet_id/calculate_rate.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'order_tag_page_3.dart';

class OrderTagPage extends StatefulWidget {
  final String petID;

  const OrderTagPage({Key key, this.petID}) : super(key: key);
  @override
  State<StatefulWidget> createState() => OrderTagPageState(petID);
}

class OrderTagPageState extends State<OrderTagPage> {
  final String petID;

  double height;
  double width;
  bool _obscureText = true;
  var orangeColor = const Color(0xffF0F3BD);
  var textColor = const Color(0xffF2F2F2);
  var listCountryname = {};

  OrderTagPageState(this.petID);

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  var dropdownValue = "value";
  final myFirstNameController = TextEditingController();
  final myEmailNameController = TextEditingController();
  final myAddressOneController = TextEditingController();
  final myAddressTwoController = TextEditingController();
  final myCityController = TextEditingController();
  final myZipController = TextEditingController();
  final myCountryController = TextEditingController();
  final myDiscountController = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  var subscribed = false;

  var firstName;
  var email;
  var lastName;
  var listCountries;

  @override
  void initState() {
    super.initState();
    asyncInitState();
  }

  void asyncInitState() async {
    getCountries().then((val) {
      /* var jsonData = json.decode(val);
*/ /*
      listCountries = jsonData["data"];
*/ /*
      listCountries = (jsonDecode(val) as List<dynamic>).cast<String>();
      print(listCountries);*/
    });
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(colors: [
              const Color(0xFF028090),
              const Color(0xFF05668D),
            ], begin: Alignment.bottomCenter, end: Alignment.centerRight),
          ),
          child: Column(
            children: <Widget>[
              Container(
                child: Column(
                  children: <Widget>[
                    Container(
                      height: height / 12,
                    ),
                    Container(
                      child: Column(
                        children: <Widget>[
                          Text(
                            "PETiD®",
                            style: TextStyle(
                                fontFamily: "Squada1",
                                fontSize: 70,
                                color: orangeColor),
                          ),
                          Text(
                            "INTELLIGENT PET IDENTIFICATION",
                            style: TextStyle(
                                fontFamily: "Squada1",
                                fontSize: 12,
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                                letterSpacing: 0.1),
                          )
                        ],
                      ),
                    ),
                    Container(
                      width: 258,
                      margin: EdgeInsets.only(
                          top: height / 30, bottom: height / 16, right: 0),
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Image.asset(
                                "assets/ptagsh.png",
                                width: 84,
                                height: 78,
                              ),
                              Container(
                                  margin: EdgeInsets.only(left: 4),
                                  width: 170,
                                  child: Text(
                                    "Tags are 1.25\" in diameter and verified lead free. These are sized for both dogs and cats of all sizes. Made in the USA.",
                                    style: TextStyle(
                                        fontSize: 12, color: Colors.white),
                                    textAlign: TextAlign.start,
                                  ))
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 32),
                            alignment: AlignmentDirectional.topStart,
                            child: Text(
                              "Shipping Info:",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                          ),
                          TextField(
                            style: new TextStyle(color: Colors.white),
                            controller: myFirstNameController,
                            decoration: InputDecoration(
                              hintText: "Name",
                              hintStyle: TextStyle(color: Colors.white),
                              enabledBorder: new UnderlineInputBorder(
                                  borderSide:
                                      new BorderSide(color: Colors.white)),
                            ),
                          ),
                          TextField(
                            style: new TextStyle(color: Colors.white),
                            controller: myEmailNameController,
                            decoration: InputDecoration(
                              hintText: "Email",
                              hintStyle: TextStyle(color: Colors.white),
                              enabledBorder: new UnderlineInputBorder(
                                  borderSide:
                                  new BorderSide(color: Colors.white)),
                            ),
                          ),

                          TextField(
                            style: new TextStyle(color: Colors.white),
                            controller: myAddressOneController,
                            decoration: InputDecoration(
                              hintText: "Address 1",
                              hintStyle: TextStyle(color: Colors.white),
                              enabledBorder: new UnderlineInputBorder(
                                  borderSide:
                                      new BorderSide(color: Colors.white)),
                            ),
                          ),
                          /*TextField(
                            style: new TextStyle(color: Colors.white),
                            controller: myAddressTwoController,
                            decoration: InputDecoration(
                              hintText: "Address 2",
                              hintStyle: TextStyle(color: Colors.white),
                              enabledBorder: new UnderlineInputBorder(
                                  borderSide:
                                      new BorderSide(color: Colors.white)),
                            ),
                          ),*/
                          TextField(
                            style: new TextStyle(color: Colors.white),
                            controller: myCityController,
                            decoration: InputDecoration(
                              hintText: "City",
                              hintStyle: TextStyle(color: Colors.white),
                              enabledBorder: new UnderlineInputBorder(
                                  borderSide:
                                  new BorderSide(color: Colors.white)),
                            ),
                          ),
                          TextField(
                            controller: myZipController,
                            style: new TextStyle(color: Colors.white),
                            decoration: InputDecoration(
                              hintText: "Zip Code",
                              hintStyle: TextStyle(color: Colors.white),
                              enabledBorder: new UnderlineInputBorder(
                                  borderSide:
                                      new BorderSide(color: Colors.white)),
                            ),
                          ),
                          /*FutureBuilder(
                            future: getCountries(),
                            builder: (context, snapshot){
                              if(snapshot.hasData){
                                var countriesJson = json.decode(snapshot.data);
                                List<String> jsonDataValue = (countriesJson as List<dynamic>).cast<String>();
                                return Center(
                                  child: */ /*new DropdownButton(
                                    items: listCountries.map<DropdownMenuItem<String>>((item) {
                                      return new DropdownMenuItem(
                                        child: new Text(item['name']),
                                        value: item['code'].toString(),
                                      );
                                    }).toList(),
                                    */ /**/ /*onChanged: (newVal) {
                                      setState(() {
                                        dropdownValue = newVal;
                                      });
                                    },*/ /**/ /*
                                    value: dropdownValue,
                                  )*/ /*
                                DropDownField(
                                  value: dropdownValue,
                                    required: true,
                                    strict: true,
                                    labelText: 'Choose Country',
                                    items: jsonDataValue,
                                    setter: (dynamic newValue) {
                                      dropdownValue = newValue;
                                    }
                                ));
                              }else{
                                return CircularProgressIndicator();
                              }
                            },
                          ),*/
                          TextField(
                            controller: myDiscountController,
                            style: new TextStyle(color: Colors.white),
                            decoration: InputDecoration(
                              hintText: "Discount Code",
                              hintStyle: TextStyle(color: Colors.white),
                              enabledBorder: new UnderlineInputBorder(
                                  borderSide:
                                      new BorderSide(color: Colors.white)),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              if (myFirstNameController.text.length > 4 &&
                                  myAddressOneController.text.length > 4 &&
                                  myEmailNameController.text.length > 4 &&
                                  myCityController.text.length > 4 &&
                                  myZipController.text.length > 4) {
                                calculateRateFun(
                                        "US",
                                        myZipController.value.text,
                                        myDiscountController.value.text)
                                    .then((value) {
                                  var jsonCalculated = json.decode(value);

                                  if (jsonCalculated["status"]) {
                                    var subtotalPay =
                                        jsonCalculated["tag_price"];
                                    var discountPrice =
                                        jsonCalculated["discount"];
                                    var shippingPrice =
                                        jsonCalculated["shipping_charge"];

                                    var totalPrice = double.parse(
                                            jsonCalculated["total_charge"]) *
                                        100;
                                    generateSKU(totalPrice.toInt())
                                        .then((skuValue) {
                                      var jsonSku = json.decode(skuValue);
                                      if (jsonSku["id"] != null) {
                                        generateOrder(jsonSku["id"])
                                            .then((orderVal) {
                                          var jsonOrder = json.decode(orderVal);
                                          if (jsonOrder["id"] != null) {
                                            var totalPay = jsonOrder["amount"];
                                            var taxAmount =
                                                jsonOrder["items"][1]["amount"];
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      OrderTagPage3(
                                                        calculateRate:
                                                            CalculateRate(
                                                                (totalPay / 100)
                                                                    .toString(),
                                                                subtotalPay
                                                                    .toString(),
                                                                (taxAmount/100)
                                                                    .toString(),
                                                                discountPrice
                                                                    .toString(),
                                                                shippingPrice
                                                                    .toString(),
                                                                jsonOrder[
                                                                    "id"]),
                                                      )),
                                            );
                                          }
                                        });
                                      }
                                    });
                                  }
                                });
                              } else {
                                Fluttertoast.showToast(
                                    msg: "Fill all the details ");
                              }
                            },
                            child: Container(
                              width: 258,
                              height: 45,
                              margin: EdgeInsets.only(top: 16),
                              color: orangeColor,
                              child: Center(
                                child: Text(
                                  "Next",
                                  style: TextStyle(
                                      fontFamily: "Squada1",
                                      color: Colors.black,
                                      fontSize: 18),
                                ),
                              ),
                            ),
                          ),
                          Container(
                            width: 258,
                            height: 32,
                            margin: EdgeInsets.only(top: 16),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.only(bottom: 10, right: 4),
                                  width: 18,
                                  height: 18,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(16),
                                      color: orangeColor),
                                ),
                                Container(
                                  margin: EdgeInsets.only(bottom: 10, right: 4),
                                  width: 18,
                                  height: 18,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(16),
                                    color: Color(0xFF4F4F4F),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(bottom: 10, right: 4),
                                  width: 18,
                                  height: 18,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(16),
                                    color: Color(0xFF4F4F4F),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(bottom: 10, right: 4),
                                  width: 18,
                                  height: 18,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(16),
                                    color: Color(0xFF4F4F4F),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                            width: 258,
                            height: 45,
                            margin: EdgeInsets.only(top: 16),
                            color: Color(0xFF4F4F4F),
                            child: Center(
                              child: Text(
                                "Exit",
                                style: TextStyle(
                                    fontFamily: "Squada1",
                                    color: Colors.white,
                                    fontSize: 18),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )),
    );
  }

  Future<String> calculateRateFun(
      String country, String postal, String discount) async {
    final prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString("access_token");
    String url = 'https://pet-id.app/deploy/public/api/calculate-rate';
    Map data = {
      'toCountry': country,
      'toPostalCode': postal,
      'discount_code': discount,
    };
    var response = await http.post(url,
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          "Accept": "application/x-www-form-urlencoded",
          "Authorization": "Bearer " + accessToken,
        },
        body: data);
    print(response.body);
    return response.body;
  }

  Future<String> generateSKU(int price) async {
    String url = 'https://api.stripe.com/v1/skus';
    Map data = {
      'price': '$price',
      'currency': 'usd',
      'product': 'prod_GNjzSmE1XlKWhr',
      'attributes[petID]': petID,
      'attributes[userId]': '1',
      'inventory[type]': 'infinite',
    };
    var response = await http.post(url,
        headers: {
          "Authorization": "Bearer " + "sk_test_AxUWnazrBOnBZDe07UYXdHSH",
        },
        body: data);
    return response.body;
  }

  Future<String> generateOrder(String skuID) async {
    String url = 'https://api.stripe.com/v1/orders';
    Map data = {
      'currency': 'usd',
      'items[0][type]': 'sku',
      'items[0][parent]': skuID,
      'email': myEmailNameController.value.text??"not@available.com",
      'shipping[name]': myFirstNameController.value.text ?? "Unknown",
      "shipping[address][line1]": myAddressOneController.value.text ?? "Unown",
      "shipping[address][city]": myCityController.value.text ?? "Unown",
      "shipping[address][country]": "US" ?? "Unown",
      "shipping[address][postal_code]": myZipController.value.text ?? "44300",
    };
    var response = await http.post(url,
        headers: {
          "Authorization": "Bearer " + "sk_test_AxUWnazrBOnBZDe07UYXdHSH",
        },
        body: data);
    return response.body;
  }

  Future<String> getCountries() async {
    String url = 'https://pet-id.app/deploy/public/api/country-list';
    var response = await http.get(
      url,
    );
    print(response.body);
    return response.body;
  }
}
