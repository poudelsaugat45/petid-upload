import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:qrcode_reader/qrcode_reader.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class ScanQrPage extends StatefulWidget {
  @override
  _ScanQrPageState createState() => _ScanQrPageState();
}

class _ScanQrPageState extends State<ScanQrPage> {
  var petList = [];
  var petName, userNo1, userNo2, userAdd1, userAdd2, petReward, userEmail, petBreed, petGender, petColor, message;
  var image1, image2;
  var hasData = false;
  ProgressDialog pr;
  @override
  void initState() {
    super.initState();
    new QRCodeReader().scan().then((val) {
      getPetDetails(val)
          .then((response) {
            pr.hide();
        var jsonDecoded = json.decode(response);
        setState(() {
          hasData = true;
          petName = jsonDecoded["pet-info"]["name"];
          petColor = jsonDecoded["pet-info"]["color"];
          petBreed = jsonDecoded["pet-info"]["breed"];
          petGender = jsonDecoded["pet-info"]["gender"];
          message = jsonDecoded["pet-info"]["message"];
          userNo1 = jsonDecoded["contact-info"]["phone1"];
          userNo2 = jsonDecoded["contact-info"]["phone2"];
          userAdd1 = jsonDecoded["contact-info"]["address1"];
          userAdd2 = jsonDecoded["contact-info"]["address2"];
          petReward = jsonDecoded["contact-info"]["reward"];
          image1 = jsonDecoded["pet-info"]["image1"];
          image2 = jsonDecoded["pet-info"]["image2"];
        });
      });
    });
  }
  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context);
    var orangeColor = const Color(0xffF0F3BD);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: PreferredSize(
          child: Container(
            padding: EdgeInsets.only(top: 32),
            color: const Color(0xff131313),
            child: Column(
              children: <Widget>[
                Center(
                  child: Text(
                    "PETiD®",
                    style: TextStyle(
                        fontFamily: "Squada1",
                        color: orangeColor, fontSize: 30, letterSpacing: 1),
                    textAlign: TextAlign.justify,
                  ),
                ),
                Center(
                  child: Text(
                    "INTELLIGENT PET IDENTIFICATION",
                    style: TextStyle(
                        fontFamily: "Squada1",
                        color: const Color(0xffffffff),
                        fontSize: 7,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1),
                    textAlign: TextAlign.justify,
                  ),
                ),
              ],
            ),
          ),
          preferredSize: Size(360, 45)),
      body: Container(
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            gradient: LinearGradient(colors: [
              const Color(0xFF028090),
              const Color(0xFF05668D),
            ], begin: Alignment.bottomCenter, end: Alignment.centerRight),
          ),
          child: hasData?Container(
            child: Column(
              children: <Widget>[
                Container(
                  color: Color(0x22000000),
                  height: MediaQuery.of(context).size.height / 2.3,
                  child: Stack(children: [
                    PageView(
                      children: <Widget>[
                        Image.network(
                          image1!=null?image1:"https://images.unsplash.com/photo-1548199973-03cce0bbc87b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80",
                          fit: BoxFit.fitHeight,
                        ),
                        Image.network(
                          image2!=null?image2:"https://images.unsplash.com/photo-1548199973-03cce0bbc87b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80",
                          fit: BoxFit.fitHeight,
                        ),
                      ],
                    ),
                    Container(
                      height: 36,
                      color: Color(0x88000000),
                      child: Center(
                        child: Text(
                          "Reported lost: 9/19/2019",
                          style: TextStyle(
                              color: Color(
                                0xFF02C39A,
                              ),
                              fontSize: 18,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    Align(
                      alignment: AlignmentDirectional.bottomCenter,
                      child: Container(
                        height: 85,
                        alignment: AlignmentDirectional.bottomCenter,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.only(bottom: 10, right: 4),
                                  width: 16,
                                  height: 16,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(16),
                                      color: orangeColor),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(bottom: 10, right: 16),
                                  width: 16,
                                  height: 16,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(16),
                                    color: Color(0xFF4F4F4F),
                                  ),
                                )
                              ],
                            ),
                            Container(
                              height: 59,
                              color: Color(0x88000000),
                              child: Center(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Image.asset(
                                      "assets/Layer_1.png",
                                      height: 40,
                                    ),
                                    Padding(
                                        padding: EdgeInsets.only(right: 16)),
                                    RichText(
                                        textAlign: TextAlign.center,
                                        text: TextSpan(
                                          style: TextStyle(
                                            fontFamily: "Squada",
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold,
                                          ),
                                          text: "You've found ",
                                          children: [
                                            TextSpan(
                                              text: petName,
                                              style: TextStyle(
                                                  color: orangeColor,
                                                  decoration:
                                                      TextDecoration.underline),
                                            ),
                                            TextSpan(
                                              text: "!",
                                            ),
                                          ],
                                        )),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ]),
                ),
                Container(
                  margin: EdgeInsets.only(top: 16),
                  width: 320,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              Text(
                                "Contact Numbers:",
                                style:
                                    TextStyle(fontSize: 16, color: orangeColor),
                              ),
                              Padding(padding: EdgeInsets.only(bottom: 10)),
                              Text(
                                userNo1,
                                style: TextStyle(decoration: TextDecoration.underline,
                                    fontSize: 16, color: Colors.white),
                              ),
                              Text(
                                userNo2,
                                style: TextStyle(decoration: TextDecoration.underline,
                                    fontSize: 16, color: Colors.white),
                              )
                            ],
                          ),
                          Column(
                            children: <Widget>[
                              Text(
                                "Return Address:",
                                style:
                                    TextStyle(fontSize: 16, color: orangeColor),
                              ),
                              Padding(padding: EdgeInsets.only(bottom: 10)),
                              Text(
                                userAdd1,
                                style: TextStyle(
                                    fontSize: 16, color: Colors.white),
                              ),
                              Text(
                                userAdd2,
                                style: TextStyle(
                                    fontSize: 16, color: Colors.white),
                              ),
                            ],
                          )
                        ],
                      ),
                      Padding(padding: EdgeInsets.only(bottom: 8)),
                      RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                            style: TextStyle(
                              fontFamily: "Squada",
                              fontSize: 16,
                              color: orangeColor,
                            ),
                            text: "Reward: ",
                            children: [
                              TextSpan(
                                text: "Yes",
                                style: TextStyle(color: Colors.white),
                              ),
                            ],
                          )),
                      Padding(padding: EdgeInsets.only(bottom: 8)),
                      RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                            style: TextStyle(
                              fontFamily: "Squada",
                              fontSize: 16,
                              color: orangeColor,
                            ),
                            text: "Email: ",
                            children: [
                              TextSpan(
                                text: userEmail,
                                style: TextStyle(color: Colors.white),
                              ),
                            ],
                          )),
                      Padding(padding: EdgeInsets.only(bottom: 8)),

                      RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                            style: TextStyle(
                              fontFamily: "Squada",
                              fontSize: 16,
                              color: orangeColor,
                            ),
                            text: "Breed: ",
                            children: [
                              TextSpan(
                                text: petBreed,
                                style: TextStyle(color: Colors.white),
                              ),
                              TextSpan(
                                text: " Sex: ",
                              ),
                              TextSpan(
                                text: petGender,
                                style: TextStyle(color: Colors.white),
                              ),
                              TextSpan(
                                text: " Color: ",
                              ),
                              TextSpan(
                                text: petColor,
                                style: TextStyle(color: Colors.white),
                              ),
                            ],
                          )),
                      Padding(padding: EdgeInsets.only(bottom: 8)),

                      RichText(
                          textAlign: TextAlign.start,
                          text: TextSpan(
                            style: TextStyle(
                              fontFamily: "Squada",
                              fontSize: 16,
                              color: orangeColor,
                            ),
                            text: "Notes: ",
                            children: [
                              TextSpan(
                                text: message,
                                style: TextStyle(color: Colors.white),
                              ),
                            ],
                          )),
                      GestureDetector(
                        child: Container(
                            width: 320,
                            height: 45,
                            margin: EdgeInsets.only(top: 24),
                            color: orangeColor,
                            child: Center(
                                child: Text(
                                  "Scan Again",
                                  style: TextStyle(
                                      fontFamily: "Squada1",
                                      fontSize: 17,
                                      fontWeight: FontWeight.w400),
                                ))),
                        onTap: () {
                          new QRCodeReader().scan().then((val) {
                            pr.show();
                            getPetDetails(val)
                                .then((response) {
                              pr.hide();
                              var jsonDecoded = json.decode(response);
                              setState(() {
                                hasData = true;
                                petName = jsonDecoded["pet-info"]["name"];
                                petColor = jsonDecoded["pet-info"]["color"];
                                petBreed = jsonDecoded["pet-info"]["breed"];
                                petGender = jsonDecoded["pet-info"]["gender"];
                                message = jsonDecoded["pet-info"]["message"];
                                userNo1 = jsonDecoded["contact-info"]["phone1"];
                                userNo2 = jsonDecoded["contact-info"]["phone2"];
                                userAdd1 = jsonDecoded["contact-info"]["address1"];
                                userAdd2 = jsonDecoded["contact-info"]["address2"];
                                petReward = jsonDecoded["contact-info"]["reward"];
                                image1 = jsonDecoded["pet-info"]["image1"];
                                image2 = jsonDecoded["pet-info"]["image2"];
                              });
                            });
                          });
                        },
                      ),
                    ],
                  ),
                )
              ],
            ),
          ):Container(
            height: MediaQuery.of(context).size.height,
            padding: EdgeInsets.only(top: 200),
            child: Align(
              alignment: AlignmentDirectional.center,
              child: Column(
                children: <Widget>[
                  Text("No Pet Found", style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),),
                  GestureDetector(
                    child: Container(
                        width: 320,
                        height: 45,
                        margin: EdgeInsets.only(top: 24),
                        color: orangeColor,
                        child: Center(
                            child: Text(
                              "Scan Again",
                              style: TextStyle(
                                  fontFamily: "Squada1",
                                  fontSize: 17,
                                  fontWeight: FontWeight.w400),
                            ))),
                    onTap: () {
                      new QRCodeReader().scan().then((val) {
                        if(val!=null && val!="No QR Code found"){
                          pr.show();
                        }
                        getPetDetails(val).then((response) {
                          pr.hide();
                          var jsonDecoded = json.decode(response);
                          setState(() {
                            hasData = true;
                            petName = jsonDecoded["pet-info"]["name"];
                            petColor = jsonDecoded["pet-info"]["color"];
                            petBreed = jsonDecoded["pet-info"]["breed"];
                            petGender = jsonDecoded["pet-info"]["gender"];
                            message = jsonDecoded["pet-info"]["message"];
                            userNo1 = jsonDecoded["contact-info"]["phone1"];
                            userNo2 = jsonDecoded["contact-info"]["phone2"];
                            userAdd1 = jsonDecoded["contact-info"]["address1"];
                            userAdd2 = jsonDecoded["contact-info"]["address2"];
                            petReward = jsonDecoded["contact-info"]["reward"];
                            image1 = jsonDecoded["pet-info"]["image1"];
                            image2 = jsonDecoded["pet-info"]["image2"];
                          });
                        });
                      });
                    },
                  ),
                ],
              ),
            ),
          )),
    );
  }

  Future<String> getPetDetails(String petId) async {
    final prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString("access_token");

    String url = 'https://pet-id.app/deploy/public/api/rfp/$petId';
    var response = await http.get(
      url,
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": "Bearer " + accessToken,
      },
    );
    print(response.body + petId);
    return response.body;
  }
}
