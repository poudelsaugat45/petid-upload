import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:pet_id/confirm_order_page.dart';
import 'package:stripe_payment/stripe_payment.dart';

class OrderTagPage2 extends StatefulWidget {
  final String orderID;
  final String totalAmount;

  const OrderTagPage2({Key key, this.orderID, this.totalAmount})
      : super(key: key);

  @override
  State<StatefulWidget> createState() =>
      OrderTagPage2State(orderID, totalAmount);
}

class OrderTagPage2State extends State<OrderTagPage2> {
  final String orderID;
  final String totalAmount;

  OrderTagPage2State(this.orderID, this.totalAmount);

  double height;
  double width;

  var orangeColor = const Color(0xffF0F3BD);
  var textColor = const Color(0xffF2F2F2);

  final myCardNoController = TextEditingController();
  final myCardExpMonthController = TextEditingController();
  final myCardExpYearController = TextEditingController();
  final myCardCVCController = TextEditingController();

  @override
  void initState() {
    super.initState();
    asyncInitState();
    StripePayment.setOptions(
      StripeOptions(
        publishableKey: "pk_test_xF8gwyqpS97z53FiZ5DFbyQc",
        androidPayMode: 'test',
      ),
    );
  }

  void asyncInitState() async {}

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(colors: [
              const Color(0xFF028090),
              const Color(0xFF05668D),
            ], begin: Alignment.bottomCenter, end: Alignment.centerRight),
          ),
          child: Column(
            children: <Widget>[
              Container(
                child: Column(
                  children: <Widget>[
                    Container(
                      height: height / 12,
                    ),
                    Container(
                      child: Column(
                        children: <Widget>[
                          Text(
                            "PETiD®",
                            style: TextStyle(
                                fontFamily: "Squada1",
                                fontSize: 70,
                                color: orangeColor),
                          ),
                          Text(
                            "INTELLIGENT PET IDENTIFICATION",
                            style: TextStyle(
                                fontFamily: "Squada1",
                                fontSize: 12,
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                                letterSpacing: 0.1),
                          )
                        ],
                      ),
                    ),
                    Container(
                      width: 258,
                      margin: EdgeInsets.only(
                          top: height / 30, bottom: height / 16, right: 0),
                      child: Column(
                        children: <Widget>[
                          Container(
                            width: 180,
                            child: Row(
                              children: <Widget>[
                                Image.asset(
                                  "assets/ptagsh.png",
                                  width: 84,
                                  height: 78,
                                ),
                                Container(
                                    padding: EdgeInsets.only(left: 16),
                                    width: 92,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          "\$ $totalAmount",
                                          style: TextStyle(
                                              fontSize: 30, color: orangeColor),
                                        ),
                                        Text(
                                          "One time fee. Per tag , per pet.",
                                          style: TextStyle(
                                              fontSize: 10,
                                              color: Colors.white),
                                        )
                                      ],
                                    ))
                              ],
                            ),
                          ),
                          TextField(
                            controller: myCardNoController,
                            style: new TextStyle(color: Colors.white),
                            inputFormatters: [
                              LengthLimitingTextInputFormatter(16),
                              WhitelistingTextInputFormatter.digitsOnly
                            ],
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              hintText: "Card Number",
                              hintStyle: TextStyle(color: Colors.white),
                              enabledBorder: new UnderlineInputBorder(
                                  borderSide:
                                      new BorderSide(color: Colors.white)),
                            ),
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                  child: TextField(
                                style: new TextStyle(color: Colors.white),
                                inputFormatters: [
                                  LengthLimitingTextInputFormatter(2),
                                  WhitelistingTextInputFormatter.digitsOnly
                                ],
                                keyboardType: TextInputType.number,
                                controller: myCardExpMonthController,
                                decoration: InputDecoration(
                                  hintText: "MM",
                                  hintStyle: TextStyle(color: Colors.white),
                                  enabledBorder: new UnderlineInputBorder(
                                      borderSide:
                                          new BorderSide(color: Colors.white)),
                                ),
                              )),
                              Expanded(
                                  child: TextField(
                                controller: myCardExpYearController,
                                inputFormatters: [
                                  LengthLimitingTextInputFormatter(2),
                                  WhitelistingTextInputFormatter.digitsOnly
                                ],
                                keyboardType: TextInputType.number,
                                style: new TextStyle(color: Colors.white),
                                decoration: InputDecoration(
                                  hintText: "YY",
                                  hintStyle: TextStyle(color: Colors.white),
                                  enabledBorder: new UnderlineInputBorder(
                                      borderSide:
                                          new BorderSide(color: Colors.white)),
                                ),
                              )),
                              Padding(
                                padding: EdgeInsets.only(right: 4),
                              ),
                              Expanded(
                                  child: TextField(
                                controller: myCardCVCController,
                                inputFormatters: [
                                  LengthLimitingTextInputFormatter(3),
                                  WhitelistingTextInputFormatter.digitsOnly
                                ],
                                keyboardType: TextInputType.number,
                                style: new TextStyle(color: Colors.white),
                                decoration: InputDecoration(
                                  hintText: "CVC",
                                  hintStyle: TextStyle(color: Colors.white),
                                  enabledBorder: new UnderlineInputBorder(
                                      borderSide:
                                          new BorderSide(color: Colors.white)),
                                ),
                              )),
                            ],
                          ),
                          GestureDetector(
                            onTap: () {
                              if (myCardNoController.value.text.length > 15 &&
                                  myCardExpMonthController.value.text.length >
                                      0 &&
                                  myCardExpYearController.value.text.length >
                                      0) {
                                CreditCard card = CreditCard(
                                    number: myCardNoController.value.text,
                                    expMonth: int.parse(
                                        myCardExpMonthController.value.text),
                                    expYear: int.parse(
                                        myCardExpYearController.value.text),
                                    cvc: myCardCVCController.value.text);
                                payStripe(card, orderID).then((success) {
                                  if (success) {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              ConfirmOrderPage()),
                                    );
                                  } else {
                                    Fluttertoast.showToast(
                                        msg: "Payment Unsuccessful");
                                  }
                                });
                              } else {
                                Fluttertoast.showToast(
                                    msg: "Fill all the details");
                              }
                            },
                            child: Container(
                              width: 258,
                              height: 45,
                              margin: EdgeInsets.only(top: 64),
                              color: orangeColor,
                              child: Center(
                                child: Text(
                                  "Next",
                                  style: TextStyle(
                                      fontFamily: "Squada1",
                                      color: Colors.black,
                                      fontSize: 18),
                                ),
                              ),
                            ),
                          ),
                          Container(
                            width: 258,
                            height: 32,
                            margin: EdgeInsets.only(top: 16),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.only(bottom: 10, right: 4),
                                  width: 18,
                                  height: 18,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(16),
                                      color: Color(0xFF4F4F4F)),
                                ),
                                Container(
                                  margin: EdgeInsets.only(bottom: 10, right: 4),
                                  width: 18,
                                  height: 18,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(16),
                                    color: Color(0xFF4F4F4F),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(bottom: 10, right: 4),
                                  width: 18,
                                  height: 18,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(16),
                                    color: orangeColor,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(bottom: 10, right: 4),
                                  width: 18,
                                  height: 18,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(16),
                                    color: Color(0xFF4F4F4F),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                            width: 258,
                            height: 45,
                            margin: EdgeInsets.only(top: 16),
                            color: Color(0xFF4F4F4F),
                            child: Center(
                              child: Text(
                                "Exit",
                                style: TextStyle(
                                    fontFamily: "Squada1",
                                    color: Colors.white,
                                    fontSize: 18),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )),
    );
  }

  Future<bool> payStripe(CreditCard card, String orderID) async {
    bool successful;
    successful = false;
    try {
      await StripePayment.createTokenWithCard(card).then((token) async {
        String url = 'https://api.stripe.com/v1/orders/$orderID/pay';
        Map data = {
          'source': token.tokenId,
        };
        var response = await http.post(url,
            headers: {
              "Authorization": "Bearer " + "sk_test_AxUWnazrBOnBZDe07UYXdHSH",
            },
            body: data);
        print(response.body);
        var jsonOrder = json.decode(response.body);
        if (jsonOrder["id"] != null) {
          successful = true;
        }
      });
    } catch (e) {
      Fluttertoast.showToast(msg: e);
    }
    return successful;
  }
}
