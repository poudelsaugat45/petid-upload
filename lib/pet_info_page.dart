import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pet_id/add_pet_details.dart';
import 'package:pet_id/pet_details_page.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PetInfoPage extends StatefulWidget {
  @override
  _PetInfoPageState createState() => _PetInfoPageState();
}

class _PetInfoPageState extends State<PetInfoPage> {
  var petList;
  //var getPets = getPetLists();
  ProgressDialog pr;

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context);
    var orangeColor = const Color(0xffF0F3BD);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: PreferredSize(
          child: Container(
            padding: EdgeInsets.only(top: 32),
            color: const Color(0xff131313),
            child: Column(
              children: <Widget>[
                Center(
                  child: Text(
                    "PETiD®",
                    style: TextStyle(
                        fontFamily: "Squada1",
                        color: orangeColor, fontSize: 30, letterSpacing: 1),
                    textAlign: TextAlign.justify,
                  ),
                ),
                Center(
                  child: Text(
                    "INTELLIGENT PET IDENTIFICATION",
                    style: TextStyle(
                        fontFamily: "Squada1",
                        color: const Color(0xffffffff),
                        fontSize: 7,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1),
                    textAlign: TextAlign.justify,
                  ),
                ),
              ],
            ),
          ),
          preferredSize: Size(360, 45)),
      body: Container(
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.only(top: 16),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            gradient: LinearGradient(colors: [
              const Color(0xFF028090),
              const Color(0xFF05668D),
            ], begin: Alignment.bottomCenter, end: Alignment.centerRight),
          ),
          child: Container(
            width: 322,
            child: FutureBuilder(
                future: getPetLists(),
                builder: (context, snapshot) {
                  if (snapshot.hasData && snapshot.connectionState==ConnectionState.done) {
                    var jsonResponse;
                    try {
                      jsonResponse = json.decode(snapshot.data);
                      if (!jsonResponse["status"]) {
                        //Fluttertoast.showToast(msg: jsonResponse['message']);
                      } else {
                          petList = jsonResponse["data"];
                      }
                    } catch (e) {

                    }
                    if (petList == null || petList.toString().trim()=="") {
                      return Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(top: 64.0),
                            child: Image.asset("assets/Capa_3.png"),
                          ),
                          GestureDetector(
                            onTap: () {

                            },
                            child: Container(
                              width: 322,
                              height: 45,
                              margin: EdgeInsets.only(top: 32),
                              color: orangeColor,
                              child: Center(
                                child: Text(
                                  "Add a Pet",
                                  style: TextStyle(
                                      fontFamily: "Squada1",
                                      fontSize: 18,
                                      fontWeight: FontWeight.w400),
                                ),
                              ),
                            ),
                          )
                        ],
                      );
                    } else if (petList.length > 0) {
                      return CustomScrollView(
                        slivers: <Widget>[
                          SliverList(
                            delegate: SliverChildBuilderDelegate(
                                (BuildContext context, int index) {
                              return GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => PetDetailsPage(
                                              petId: petList[index]["id"]
                                                  .toString(),
                                            )),
                                  ).then((detVal){
                                    getPetLists().then((snapVal){
                                      setState(() {
                                      });
                                    });
                                  });
                                },
                                child: Container(
                                  height: 96,
                                  width: 322,
                                  margin: EdgeInsets.only(
                                      top: 8, left: 32, right: 32, bottom: 8),
                                  color: Color(0x55333333),
                                  child: Row(
                                    children: <Widget>[
                                      Image.network(
                                        petList[index]["image1"],
                                        width: 88,
                                        height: 88,
                                        fit: BoxFit.fitHeight,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(16.0),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              petList[index]["name"],
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 20),
                                            ),
                                            RichText(
                                              textAlign: TextAlign.center,
                                              text: TextSpan(
                                                  style: TextStyle(
                                                    fontFamily: "Squada",
                                                    fontSize: 13,
                                                  ),
                                                  text: "Status: ",
                                                  children: [
                                                    TextSpan(
                                                      text: petList[index]
                                                          ["status"]=="0"?"Lost":"Safe",
                                                      style: TextStyle(
                                                          color: petList[index][
                                                                      "status"] == "0"
                                                              ? Color(
                                                                  0xFF02C39A)
                                                              : orangeColor),
                                                    ),
                                                  ]),
                                            ),
                                            Text(
                                              "Lost Since: ${petList[index]["status_verified_at"]}",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 10),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                          child: Align(
                                              alignment: AlignmentDirectional
                                                  .centerEnd,
                                              child: Icon(
                                                Icons.arrow_forward_ios,
                                                color: Colors.white,
                                                size: 34,
                                              )))
                                    ],
                                  ),
                                ),
                              );
                            }, childCount: petList.length),
                          ),
                          SliverToBoxAdapter(
                            child: InkWell(
                              onTap: (){
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => AddPetPage()),
                                ).then((vaal){
                                  getPetLists().then((snapVal){
                                  });
                                });
                              },
                              child: Container(
                                width: 322,
                                height: 45,
                                margin:
                                    EdgeInsets.only(top: 32, left: 32, right: 32),
                                color: orangeColor,
                                child: Center(
                                  child: Text(
                                    "Add a Pet",
                                    style: TextStyle(
                                        fontFamily: "Squada1",
                                        fontSize: 18,
                                        fontWeight: FontWeight.w400),
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      );
                    }
                    else {
                      return Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(top: 64.0),
                            child: Image.asset("assets/Capa_3.png"),
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => AddPetPage()),
                              ).then((val){
                                getPetLists().then((snapVal){
                                });
                              });
                            },
                            child: Container(
                              width: 322,
                              height: 45,
                              margin: EdgeInsets.only(top: 32),
                              color: orangeColor,
                              child: Center(
                                child: Text(
                                  "Add a Pet",
                                  style: TextStyle(
                                      fontFamily: "Squada1",
                                      fontSize: 18,
                                      fontWeight: FontWeight.w400),
                                ),
                              ),
                            ),
                          )
                        ],
                      );
                    }
                  } else {
                    return Center(child: CircularProgressIndicator());
                  }
                }),
          )),
    );
  }
}

Future<String> getPetLists() async {
  final prefs = await SharedPreferences.getInstance();
  String accessToken = prefs.getString("access_token");
  print(accessToken);
  String url = 'https://pet-id.app/deploy/public/api/my-pets';
  var response = await http.get(
    url,
    headers: {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer " + accessToken,
    },
  );
  print(response.body);
  return response.body;
}
