import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pet_id/home_page.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class OnboardingPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => OnboardingPageState();
}

class OnboardingPageState extends State<OnboardingPage> {
  double height;
  double width;
  PageController _pageController;
  int currentPage = 0;
  bool yesReward = false;
  var orangeColor = const Color(0xffF0F3BD);
  var dotColor = const Color(0xff4f4f4f);

  final myNameController = TextEditingController();
  final myEmailController = TextEditingController();
  final myPhoneController = TextEditingController();
  final myPhoneController2 = TextEditingController();
  final myAddressController = TextEditingController();
  final myAddressController2 = TextEditingController();
  final myCityController = TextEditingController();
  final myStateController = TextEditingController();
  final myZipController = TextEditingController();
  final myCountryController = TextEditingController();
  final myMessageController = TextEditingController();
  var name, email;
  @override
  void initState() {
    super.initState();
    _pageController = PageController(initialPage: currentPage);
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final SignupArguments args = ModalRoute.of(context).settings.arguments;
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;
    setState(() {
      name = args.firstName;
      email = args.emailAddress;
    });
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [
                  const Color(0xFF028090),
                  const Color(0xFF05668D),
                ],
                begin: Alignment.bottomCenter,
                end: Alignment.centerRight),
          ),
          child: Column(
            children: <Widget>[
              Container(
                child: Column(
                  children: <Widget>[
                    Container(
                      child: Column(
                        children: <Widget>[
                          Container(
                            height: height / 12,
                          ),
                          Container(
                            child: Column(
                              children: <Widget>[
                                Text(
                                  "PETiD®",
                                  style: TextStyle(
                                      fontFamily: "Squada1",
                                      fontSize: 70,
                                      color: orangeColor),
                                ),
                                Text(
                                  "INTELLIGENT PET IDENTIFICATION",
                                  style: TextStyle(
                                      fontFamily: "Squada1",
                                      fontSize: 12,
                                      color: Colors.white,
                                      fontWeight: FontWeight.w700,
                                      letterSpacing: 0.1),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.only(
                            top: height / 32,
                            bottom: height / 32,
                            left: width / 6,
                            right: width / 6),
                        height: height / 2.1,
                        width: width,
                        child: Scaffold(
                          resizeToAvoidBottomInset: false,
                          backgroundColor: Colors.transparent,
                          body: Stack(
                            fit: StackFit.loose,
                            children: <Widget>[
                              PageView.builder(
                                itemCount: 5,
                                controller: _pageController,
                                onPageChanged: (index) {
                                  setState(() {
                                    currentPage = index;
                                  });
                                },
                                itemBuilder: (BuildContext context, int index) {
                                  return Stack(
                                    fit: StackFit.loose,
                                    children: <Widget>[
                                      AnimatedBuilder(
                                          animation: _pageController,
                                          builder: (context, child) {
                                            var first_name = args.firstName;

                                            switch (index) {
                                              case 0:
                                                return Container(
                                                  width: 259,
                                                  child: Column(
                                                    mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .start,
                                                    children: <Widget>[
                                                      Text(
                                                        "Congratulations \nLet's get started by setting your recovery info. This can always be changed.",
                                                        style: TextStyle(
                                                            fontSize: 15,
                                                            color:
                                                            Colors.white,
                                                        fontWeight: FontWeight.bold),
                                                      ),
                                                      Padding(padding: EdgeInsets.only(bottom: 24)),
                                                      Image.asset(
                                                        "assets/Capa_1.png",
                                                        height: 119,
                                                        width: 184,
                                                      )
                                                    ],
                                                  ),
                                                );
                                              case 1:
                                                return Container(
                                                  width: 259,
                                                  child: Column(
                                                    children: <Widget>[
                                                        Text(
                                                        "Does your name and email look correct?",
                                                        textAlign: TextAlign.left,
                                                        style: TextStyle(
                                                            fontSize: 15,
                                                            color:
                                                            Colors.white,
                                                        fontWeight: FontWeight.bold
                                                        ),
                                                      ),
                                                      Padding(padding: EdgeInsets.only(bottom: 24),),
                                                      TextField(
                                                        controller: myNameController,
                                                        style: new TextStyle(
                                                            color:
                                                            Colors.white),
                                                        decoration:
                                                        InputDecoration(
                                                          hintText: name,
                                                          hintStyle: TextStyle(
                                                              color: Colors
                                                                  .white),
                                                          enabledBorder: new UnderlineInputBorder(
                                                              borderSide:
                                                              new BorderSide(
                                                                  color: Colors
                                                                      .white)),
                                                        ),

                                                      ),
                                                      TextField(
                                                        controller: myEmailController,
                                                        style: new TextStyle(
                                                            color:
                                                            Colors.white),
                                                        decoration:
                                                        InputDecoration(
                                                          hintText: email,
                                                          hintStyle: TextStyle(
                                                              color: Colors
                                                                  .white),
                                                          enabledBorder: new UnderlineInputBorder(
                                                              borderSide:
                                                              new BorderSide(
                                                                  color: Colors
                                                                      .white)),
                                                        ),
                                                      ),
                                                      Padding(
                                                        padding:
                                                        const EdgeInsets.only(
                                                            top: 16.0),
                                                        child: Image.asset(
                                                          "assets/Capa_2.png",
                                                          height: 81,
                                                          width: 88,
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                );
                                              case 2:
                                                return Container(
                                                  width: 259,
                                                  child: Column(
                                                    children: <Widget>[
                                                      Text(
                                                        "Now add a few phone numbers that can be used in the event your pet is found. These should be your number and friends and family.",
                                                        style: TextStyle(
                                                            fontSize: 15,
                                                            color:
                                                            Colors.white, fontWeight: FontWeight.bold),
                                                      ),
                                                      Padding(padding: EdgeInsets.only(bottom: 24)),
                                                      TextField(
                                                        controller:
                                                        myPhoneController,
                                                        style: new TextStyle(
                                                            color:
                                                            Colors.white),
                                                        decoration:
                                                        InputDecoration(
                                                            hintText:
                                                            "Number 1",
                                                            hintStyle: TextStyle(
                                                                color: Colors
                                                                    .white),
                                                            enabledBorder: new UnderlineInputBorder(
                                                                borderSide:
                                                                new BorderSide(
                                                                    color:
                                                                    Colors.white)),
                                                            suffixIcon: Icon(
                                                              Icons.add,
                                                              color: Colors
                                                                  .white,
                                                            )),
                                                      ),
                                                      TextField(
                                                        controller:
                                                        myPhoneController2,
                                                        style: new TextStyle(
                                                            color:
                                                            Colors.white),
                                                        decoration:
                                                        InputDecoration(
                                                            hintText:
                                                            "Number 2",
                                                            hintStyle: TextStyle(
                                                                color: Colors
                                                                    .white),
                                                            enabledBorder: new UnderlineInputBorder(
                                                                borderSide:
                                                                new BorderSide(
                                                                    color:
                                                                    Colors.white)),
                                                            suffixIcon: Icon(
                                                              Icons.add,
                                                              color: Colors
                                                                  .white,
                                                            )),
                                                      ),
                                                    ],
                                                  ),
                                                );
                                              case 3:
                                                return Column(
                                                  children: <Widget>[
                                                    Text(
                                                      "Would you like to offer a reward to anybody that returns your device?",
                                                      style: TextStyle(
                                                          fontSize: 15,
                                                          color:
                                                          Colors.white, fontWeight: FontWeight.bold),
                                                    ),
                                                    Padding(padding: EdgeInsets.only(bottom: 24)),
                                                    Row(
                                                      children: <Widget>[
                                                        GestureDetector(
                                                          child: Padding(
                                                            padding:
                                                            const EdgeInsets
                                                                .only(right:8.0),
                                                            child: Text(
                                                              "Yes",
                                                              style: TextStyle(
                                                                  color: yesReward
                                                                      ? orangeColor
                                                                      : Colors
                                                                      .white,
                                                                  fontSize:
                                                                  20, fontWeight: FontWeight.bold),
                                                            ),
                                                          ),
                                                          onTap: () {
                                                            setState(() {
                                                              yesReward =
                                                              true;
                                                            });
                                                          },
                                                        ),
                                                        GestureDetector(
                                                          child: Padding(
                                                            padding:
                                                            const EdgeInsets
                                                                .only(right:8.0),
                                                            child: Text(
                                                             "No",
                                                              style: TextStyle(
                                                                  color: yesReward
                                                                      ? Colors
                                                                      .white
                                                                      : orangeColor,
                                                                  fontSize:
                                                                  20,
                                                              fontWeight: FontWeight.bold),
                                                            ),
                                                          ),
                                                          onTap: () {
                                                            setState(() {
                                                              yesReward =
                                                              false;
                                                            });
                                                          },
                                                        ),
                                                      ],
                                                    ),
                                                    Padding(
                                                      padding:
                                                      const EdgeInsets.only(
                                                          top: 32.0),
                                                      child: Image.asset(
                                                        "assets/Layer_1.png",
                                                        height: 120,
                                                        width: 120,
                                                      ),
                                                    )
                                                  ],
                                                );
                                              case 4:
                                                return Container(

                                                  width: 329,
                                                  child: Column(
                                                    children: <Widget>[
                                                      Text(
                                                        "Add an address that can be used to return your pet. This will also be the address we will send your tags to.",
                                                        style: TextStyle(
                                                            fontSize: 15,
                                                            fontWeight: FontWeight.bold,
                                                            color:
                                                            Colors.white),
                                                      ),
                                                      Padding(padding: EdgeInsets.only(bottom: 24),),
                                                      TextField(
                                                        controller: myAddressController,
                                                        style: new TextStyle(
                                                            color:
                                                            Colors.white),
                                                        decoration:
                                                        InputDecoration(
                                                          hintText: "Address 1",
                                                          hintStyle: TextStyle(
                                                              color: Colors
                                                                  .white),
                                                          enabledBorder: new UnderlineInputBorder(
                                                              borderSide:
                                                              new BorderSide(
                                                                  color: Colors
                                                                      .white)),
                                                        ),
                                                      ),
                                                      TextField(
                                                        controller: myAddressController2,
                                                        style: new TextStyle(
                                                            color:
                                                            Colors.white),
                                                        decoration:
                                                        InputDecoration(
                                                          hintText: "Address 2",
                                                          hintStyle: TextStyle(
                                                              color: Colors
                                                                  .white),
                                                          enabledBorder: new UnderlineInputBorder(
                                                              borderSide:
                                                              new BorderSide(
                                                                  color: Colors
                                                                      .white)),
                                                        ),
                                                      ),
                                                      TextField(
                                                        controller: myCityController,
                                                        style: new TextStyle(
                                                            color:
                                                            Colors.white),
                                                        decoration:
                                                        InputDecoration(
                                                          hintText: "City",
                                                          hintStyle: TextStyle(
                                                              color: Colors
                                                                  .white),
                                                          enabledBorder: new UnderlineInputBorder(
                                                              borderSide:
                                                              new BorderSide(
                                                                  color: Colors
                                                                      .white)),
                                                        ),
                                                      ),
                                                      TextField(
                                                        controller: myStateController,
                                                        style: new TextStyle(
                                                            color:
                                                            Colors.white),
                                                        decoration:
                                                        InputDecoration(
                                                          hintText: "State / Province / Region",
                                                          hintStyle: TextStyle(
                                                              color: Colors
                                                                  .white),
                                                          enabledBorder: new UnderlineInputBorder(
                                                              borderSide:
                                                              new BorderSide(
                                                                  color: Colors
                                                                      .white)),
                                                        ),
                                                      ),
                                                      TextField(
                                                        controller:myZipController ,
                                                        style: new TextStyle(
                                                            color:
                                                            Colors.white),
                                                        decoration:
                                                        InputDecoration(
                                                          hintText: "Zip / Postal Code",
                                                          hintStyle: TextStyle(
                                                              color: Colors
                                                                  .white),
                                                          enabledBorder: new UnderlineInputBorder(
                                                              borderSide:
                                                              new BorderSide(
                                                                  color: Colors
                                                                      .white)),
                                                        ),
                                                      ),
                                                      TextField(
                                                        controller: myCountryController,
                                                        style: new TextStyle(
                                                            color:
                                                            Colors.white),
                                                        decoration:
                                                        InputDecoration(
                                                          hintText: "Country",
                                                          hintStyle: TextStyle(
                                                              color: Colors
                                                                  .white),
                                                          enabledBorder: new UnderlineInputBorder(
                                                              borderSide:
                                                              new BorderSide(
                                                                  color: Colors
                                                                      .white)),
                                                        ),
                                                      ),

                                                    ],
                                                  ),
                                                );

                                              default:
                                                return Container();
                                            }
                                          })
                                    ],
                                  );
                                },
                              )
                            ],
                          ),
                        )),
                    GestureDetector(
                      onTap: () {
                        if (currentPage < 4) {
                          setState(() {
                            _pageController.nextPage(
                                duration: Duration(milliseconds: 500),
                                curve: Cubic(0, 0, 0, 0)
                            );
                          });
                        } else {
                          updateRecoveryData().then((v) {

                            var jsonResponse = json.decode(v);
                            if (jsonResponse['errors']!=null && jsonResponse['errors'].length>0) {
                              Fluttertoast.showToast(msg: jsonResponse['errors'].toString());
                            }else{
                              Navigator.pop(context);
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        HomePage()),
                              );
                            }
                          });

                        }
                      },
                      child: Container(
                          margin: EdgeInsets.only(
                              top: height / 48,
                              bottom: height / 48,
                              left: width / 6,
                              right: width / 6),
                          height: 45,
                          width: 259,
                          color: orangeColor,
                          child: Center(
                              child: Text(
                                "Next",
                                style: TextStyle(fontSize: 18,),
                              ))),
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            width: 32,
                            height: 20,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: currentPage == 0
                                    ? orangeColor
                                    : dotColor),
                          ),
                          Container(
                            width: 32,
                            height: 20,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: currentPage == 1
                                    ? orangeColor
                                    : dotColor),
                          ),
                          Container(
                            width: 32,
                            height: 20,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: currentPage == 2
                                    ? orangeColor
                                    : dotColor),
                          ),
                          Container(
                            width: 32,
                            height: 20,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: currentPage == 3
                                    ? orangeColor
                                    : dotColor),
                          ),
                          Container(
                            width: 32,
                            height: 20,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: currentPage == 4
                                    ? orangeColor
                                    : dotColor),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )),
    );
  }

  Future<String> updateRecoveryData() async {
    final prefs = await SharedPreferences.getInstance();
    var valDropdown;
    if (yesReward) {
      valDropdown = 1;
    } else {
      valDropdown = 0;
    }
    String accessToken = prefs.getString("access_token");
    Map data = {
      'name': myNameController.text.trim()==""?name:myNameController.text,
      'email': myEmailController.text.trim()==""?email:myEmailController.text,
      'phone1': myPhoneController.text,
      'phone2': myPhoneController2.text,
      'address1': myAddressController.text,
      'address2': myAddressController2.text,
      'address2': myAddressController2.text,
      'city': myCityController.text,
      'zip': myZipController.text,
      'state': myStateController.text,
      'country': myCountryController.text,
      'reward': yesReward?1:0,

    };
    var bodyJson = json.encode(data);
    String url = 'https://pet-id.app/deploy/public/api/contact-info';
    var response = await http.post(url,
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
          "Authorization": "Bearer " + accessToken,
        },
        body: bodyJson);
    print(response.body);
    return response.body;
  }
}

class SignupArguments {
  final String firstName;
  final String emailAddress;

  SignupArguments(this.firstName, this.emailAddress);

}
