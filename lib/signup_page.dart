import 'dart:convert';
import 'dart:io';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:flutter_inapp_purchase/flutter_inapp_purchase.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:pet_id/onboarding_screen.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SignupPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SignupPageState();
}

class SignupPageState extends State<SignupPage> {
  double height;
  double width;
  bool _obscureText = true;
  var orangeColor = const Color(0xffF0F3BD);
  var textColor = const Color(0xffF2F2F2);
  ProgressDialog pr;

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  final myFirstNameController = TextEditingController();
  final myLastNameController = TextEditingController();
  final myPhoneController = TextEditingController();
  final myUserNameController = TextEditingController();
  final myPasswordController = TextEditingController();
  final myPasswordAgainController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  var subscribed = false;

  var firstName;
  var email;
  var lastName;

  @override
  void initState() {
    super.initState();
    asyncInitState();
  }

  void asyncInitState() async {
    await FlutterInappPurchase.initConnection;
  }

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context);
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(colors: [
              const Color(0xFF028090),
              const Color(0xFF05668D),
            ], begin: Alignment.bottomCenter, end: Alignment.centerRight),
          ),
          child: Column(
            children: <Widget>[
              Container(
                child: Column(
                  children: <Widget>[
                    Container(
                      height: height / 12,
                    ),
                    Container(
                      child: Column(
                        children: <Widget>[
                          Text(
                            "PETiD®",
                            style: TextStyle(
                                fontFamily: "Squada1",
                                fontSize: 70,
                                color: orangeColor),
                          ),
                          Text(
                            "INTELLIGENT PET IDENTIFICATION",
                            style: TextStyle(
                                fontFamily: "Squada1",
                                fontSize: 12,
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                                letterSpacing: 0.1),
                          )
                        ],
                      ),
                    ),
                    Container(
                      width: 258,
                      margin: EdgeInsets.only(
                          top: height / 30, bottom: height / 16, right: 0),
                      child: Align(
                        alignment: AlignmentDirectional.centerStart,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Welcome, \nChoose an option to continue.",
                              style: TextStyle(
                                  color: textColor,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.left,
                            ),
                            Padding(padding: EdgeInsets.all(8)),
                            TextField(
                              controller: myFirstNameController,
                              style: new TextStyle(color: Colors.white),
                              decoration: InputDecoration(
                                hintText: "Name",
                                hintStyle: TextStyle(
                                    color: textColor,
                                    fontWeight: FontWeight.bold),
                                enabledBorder: new UnderlineInputBorder(
                                    borderSide:
                                        new BorderSide(color: Colors.white)),
                              ),
                            ),
                            Padding(padding: EdgeInsets.all(2)),
                            TextField(
                              controller: myUserNameController,
                              style: new TextStyle(color: Colors.white),
                              decoration: InputDecoration(
                                hintText: "Email",
                                hintStyle: TextStyle(
                                    color: textColor,
                                    fontWeight: FontWeight.bold),
                                enabledBorder: new UnderlineInputBorder(
                                    borderSide:
                                        new BorderSide(color: Colors.white)),
                              ),
                            ),
                            Padding(padding: EdgeInsets.all(2)),
                            TextField(
                              controller: myPasswordController,
                              style: new TextStyle(color: Colors.white),
                              obscureText: _obscureText,
                              decoration: InputDecoration(
                                  hintText: "Password",
                                  hintStyle: TextStyle(
                                      color: textColor,
                                      fontWeight: FontWeight.bold),
                                  enabledBorder: new UnderlineInputBorder(
                                      borderSide:
                                          new BorderSide(color: Colors.white)),
                                  suffixIcon: IconButton(
                                      icon: _obscureText
                                          ? Icon(
                                              Icons.visibility,
                                              color: Colors.white,
                                            )
                                          : Icon(
                                              Icons.visibility_off,
                                              color: Colors.white,
                                            ),
                                      onPressed: () {
                                        _toggle();
                                      })),
                            ),
                            Padding(padding: EdgeInsets.all(2)),
                            TextField(
                              controller: myPasswordAgainController,
                              style: new TextStyle(color: Colors.white),
                              obscureText: _obscureText,
                              decoration: InputDecoration(
                                  hintText: "Password Again",
                                  hintStyle: TextStyle(
                                      color: textColor,
                                      fontWeight: FontWeight.bold),
                                  enabledBorder: new UnderlineInputBorder(
                                      borderSide:
                                          new BorderSide(color: Colors.white)),
                                  suffixIcon: IconButton(
                                      icon: _obscureText
                                          ? Icon(
                                              Icons.visibility,
                                              color: Colors.white,
                                            )
                                          : Icon(
                                              Icons.visibility_off,
                                              color: Colors.white,
                                            ),
                                      onPressed: () {
                                        _toggle();
                                      })),
                            ),
                            Padding(padding: EdgeInsets.all(6)),
                            /*Center(
                                child: Text(
                              "14 day free trial then \$2.99 per month per pet. \nID tags purchased seprately in app.",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 12),
                              textAlign: TextAlign.center,
                            )),*/
                            Container(
                              margin: EdgeInsets.only(top: height / 50),
                              child: RichText(
                                textAlign: TextAlign.center,
                                text: TextSpan(
                                    style: TextStyle(
                                      fontFamily: "Squada",
                                      fontSize: 12,
                                      color: textColor,
                                    ),
                                    text:
                                    "This subscription automatically renews for \$2.99 per month after the 14 day free trial. You cancel anytime. By signing up for a free trial, you agreeto PETiD's ",
                                    children: [
                                      TextSpan(
                                        recognizer: new TapGestureRecognizer()
                                          ..onTap = () {
                                            Navigator.pushReplacementNamed(
                                                context, '/terms');
                                          },
                                        text: "Terms Of Service",
                                        style: TextStyle(
                                            decoration:
                                            TextDecoration.underline,
                                            color: orangeColor),
                                      ),
                                      TextSpan(
                                        text: " and ",
                                      ),
                                      TextSpan(
                                          text: "Privacy Policy",
                                          style: TextStyle(
                                              decoration:
                                              TextDecoration.underline,
                                              color: orangeColor),
                                          recognizer: new TapGestureRecognizer()
                                            ..onTap = () {
                                              Navigator.pushReplacementNamed(
                                                  context, '/privacy');
                                            }),
                                    ]),
                              ),
                            ),
                            Padding(padding: EdgeInsets.all(4)),
                            GestureDetector(
                              child: Container(
                                  width: 258,
                                  height: 45,
                                  color: orangeColor,
                                  child: Center(
                                      child: Text(
                                    "Subscribe Now",
                                    style: TextStyle(
                                        fontFamily: "Squada1",
                                        fontSize: 17,
                                        fontWeight: FontWeight.w400),
                                  ))),
                              onTap: () {
                                if (myFirstNameController.value.text.length >
                                        1 &&
                                    myUserNameController.value.text.length >
                                        1 &&
                                    myPasswordAgainController
                                            .value.text.length >
                                        6 &&
                                    myPasswordController.value.text.length >
                                        6) {
                                  if (myPasswordController.value.text ==
                                      myPasswordAgainController.value.text) {
                                    if (myFirstNameController.value.text
                                            .split(" ")
                                            .length <
                                        2) {
                                      Fluttertoast.showToast(
                                          msg: "Please enter full name");
                                    } else {
                                      pr.show();
                                      _buyProduct().then((nValue) {
                                        if (nValue == 1) {
                                          trySignUp().then((value) {
                                            pr.hide().then((nav) {
                                              var jsonDarta =
                                                  json.decode(value);
                                              pr.hide();
                                              decideLogin(
                                                  value,
                                                  myFirstNameController.text,
                                                  myUserNameController.text);
                                            });
                                          });
                                        } else {
                                          Fluttertoast.showToast(
                                              msg:
                                                  "Subscription payment required");
                                          pr.hide();
                                        }
                                      });
                                    }
                                  } else {
                                    Fluttertoast.showToast(
                                        msg: "Passwords do not match");
                                  }
                                } else {
                                  Fluttertoast.showToast(
                                      msg:
                                          "All fields necessory, passwords must contain at least 6 letters.");
                                }
                              },
                            ),
                            Padding(padding: EdgeInsets.all(8)),
                         /*   InkWell(
                              onTap: () {
                                _buyProduct().then((nValue) {
                                  if (nValue == 1) {
                                    loginFacebook().then((val) {
                                      trySignUpFacebook(val).then((res) {
                                        if (!Platform.isIOS) {
                                          Navigator.pop(context);
                                        }
                                        var jsonObj = json.decode(res);
                                        if (jsonObj['errors'] != null) {
                                          Fluttertoast.showToast(
                                              msg: jsonObj['errors']['email']
                                                  .toString());
                                        } else {
                                          decideLogin(
                                            res,
                                            firstName,
                                            email,
                                          );
                                        }
                                      });
                                    });
                                  }
                                });
                              },
                              child: Container(
                                  width: 258,
                                  height: 45,
                                  color: Colors.blue[900],
                                  child: Center(
                                      child: Text(
                                    "Signup with Facebook",
                                    style: TextStyle(
                                        fontFamily: "Squada1",
                                        fontSize: 17,
                                        color: textColor,
                                        fontWeight: FontWeight.w400),
                                  ))),
                            ),*/
                            Padding(padding: EdgeInsets.all(8)),
                            Container(
                              alignment: AlignmentDirectional.center,
                              child: RichText(
                                textAlign: TextAlign.center,
                                text: TextSpan(
                                    text: 'Already have an account? ',
                                    style: TextStyle(
                                        fontFamily: "Squada",
                                        fontSize: 15,
                                        color: textColor,
                                        fontWeight: FontWeight.bold),
                                    children: [
                                      TextSpan(
                                          text: "Login",
                                          style: TextStyle(
                                              fontSize: 14, color: orangeColor),
                                          recognizer: TapGestureRecognizer()
                                            ..onTap = () {
                                              Navigator.pushReplacementNamed(
                                                context,
                                                "/login",
                                              );
                                            }),
                                    ]),
                              ),
                            ),
                           /* Container(
                              margin: EdgeInsets.only(top: height / 50),
                              child: RichText(
                                textAlign: TextAlign.center,
                                text: TextSpan(
                                    style: TextStyle(
                                      fontFamily: "Squada",
                                      fontSize: 12,
                                      color: textColor,
                                    ),
                                    text:
                                        "This subscription automatically renews for \$2.99 per month after the 14 day free trial. You cancel anytime. By signing up for a free trial, you agreeto PETiD's ",
                                    children: [
                                      TextSpan(
                                        recognizer: new TapGestureRecognizer()
                                          ..onTap = () {
                                            Navigator.pushReplacementNamed(
                                                context, '/terms');
                                          },
                                        text: "Terms Of Service",
                                        style: TextStyle(
                                            decoration:
                                                TextDecoration.underline,
                                            color: orangeColor),
                                      ),
                                      TextSpan(
                                        text: " and ",
                                      ),
                                      TextSpan(
                                          text: "Privacy Policy",
                                          style: TextStyle(
                                              decoration:
                                                  TextDecoration.underline,
                                              color: orangeColor),
                                          recognizer: new TapGestureRecognizer()
                                            ..onTap = () {
                                              Navigator.pushReplacementNamed(
                                                  context, '/privacy');
                                            }),
                                    ]),
                              ),
                            ),*/
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )),
    );
  }

  Future<String> trySignUp() async {
    String url = 'https://pet-id.app/deploy/public/api/auth/register';
    Map data = {
      'name': '${myFirstNameController.text}',
      'email': '${myUserNameController.text}',
      'password': '${myPasswordController.text}',
      'password_confirmation': '${myPasswordAgainController.text}'
    };
    var bodyJson = json.encode(data);
    var response = await http.post(url,
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json"
        },
        body: bodyJson);
    print(response.body);
    return response.body;
  }

  decideLogin(String value, String name, String email) {
    var jsonResponse = json.decode(value);
    if (jsonResponse["access_token"] == null) {
      Fluttertoast.showToast(msg: "Error while signing up");
    } else {
      saveAccessToken(jsonResponse["access_token"]);
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          settings: RouteSettings(
            arguments: SignupArguments(name, email),
          ),
          builder: (context) => OnboardingPage(),
        ),
      );
    }
  }

  saveAccessToken(String accessToken) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString("access_token", accessToken);
  }

  @override
  void dispose() {
    myUserNameController.dispose();
    myPasswordController.dispose();
    myPasswordAgainController.dispose();
    super.dispose();
  }/*

  Future<String> trySignUpFacebook(var result) async {
    final graphResponse = await http.get(
        'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=$result');
    final profile = json.decode(graphResponse.body);
    setState(() {
      firstName = profile['first_name'];
      lastName = profile['last_name'];
      email = profile['email'];
    });
    String url = 'https://pet-id.app/deploy/public/api/auth/signup/fb';
    print(profile['id']);
    Map data = {
      'name': profile['first_name'],
      'email': profile['email'],
      'provider_id': profile['id'],
      'provider': 'Facebook'
    };
    var bodyJson = json.encode(data);
    var response = await http.post(url,
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json"
        },
        body: bodyJson);
    print("responsefacebook" + response.body);
    return response.body;
  }

  Future<String> loginFacebook() async {
    final facebookLogin = FacebookLogin();
    facebookLogin.loginBehavior = FacebookLoginBehavior.webViewOnly;
    final result = await facebookLogin.logIn(['email']);

    if (result.status == FacebookLoginStatus.loggedIn) {
      return result.accessToken.token;
    } else if (result.status == FacebookLoginStatus.cancelledByUser) {
      Fluttertoast.showToast(msg: "Cancelled User");
      return "cancelled";
    } else {
      Fluttertoast.showToast(msg: "Error");
      return "Error";
    }
  }*/

  Future<int> _buyProduct() async {
    int bought;
    final List<String> _productLists = Platform.isAndroid
        ? ["monthly_petid_subs"]
        : [' monthly_petid_subs '];
    await FlutterInappPurchase.getSubscriptions(_productLists)
        .then((val) async {
      try {
        PurchasedItem purchased =
            await FlutterInappPurchase.buySubscription(val[0].productId);
        Fluttertoast.showToast(msg: 'Purchase Successful');
        bought = 1;
        return 1;
      } catch (error) {
        Fluttertoast.showToast(msg: 'You must subscribe first');
        bought = 0;
        return 0;
      }
    });
    return bought;
  }
}
