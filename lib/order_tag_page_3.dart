import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pet_id/calculate_rate.dart';
import 'package:pet_id/order_tag_page_2.dart';
import 'package:stripe_payment/stripe_payment.dart';

class OrderTagPage3 extends StatelessWidget {
  final CalculateRate calculateRate;

  OrderTagPage3({Key key, @required this.calculateRate}) : super(key: key);

  double height;
  double width;
  var orangeColor = const Color(0xffF0F3BD);
  var textColor = const Color(0xffF2F2F2);

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(colors: [
              const Color(0xFF028090),
              const Color(0xFF05668D),
            ], begin: Alignment.bottomCenter, end: Alignment.centerRight),
          ),
          child: Column(
            children: <Widget>[
              Container(
                child: Column(
                  children: <Widget>[
                    Container(
                      height: height / 12,
                    ),
                    Container(
                      child: Column(
                        children: <Widget>[
                          Text(
                            "PETiD®",
                            style: TextStyle(
                                fontFamily: "Squada1",
                                fontSize: 70,
                                color: orangeColor),
                          ),
                          Text(
                            "INTELLIGENT PET IDENTIFICATION",
                            style: TextStyle(
                                fontFamily: "Squada1",
                                fontSize: 12,
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                                letterSpacing: 0.1),
                          )
                        ],
                      ),
                    ),
                    Container(
                      width: 258,
                      margin: EdgeInsets.only(
                          top: height / 30, bottom: height / 16, right: 0),
                      child: Column(
                        children: <Widget>[
                          Container(
                            width: 180,
                            child: Row(
                              children: <Widget>[
                                Image.asset(
                                  "assets/ptagsh.png",
                                  width: 84,
                                  height: 78,
                                ),
                                Container(
                                    padding: EdgeInsets.only(left: 16),
                                    width: 92,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          "\$ ${calculateRate.totalPay}",
                                          style: TextStyle(
                                              fontSize: 30, color: orangeColor),
                                        ),
                                        Text(
                                          "One time fee. Per tag , per pet.",
                                          style: TextStyle(
                                              fontSize: 10,
                                              color: Colors.white),
                                        )
                                      ],
                                    ))
                              ],
                            ),
                          ),
                          Container(
                            child: Column(
                              children: <Widget>[
                                Container(
                                  height: 1,
                                  color: Colors.white,
                                  margin: EdgeInsets.only(top: 16, bottom: 8),
                                ),
                                Row(
                                  children: <Widget>[
                                    Text(
                                      "Subtotal:",
                                      style: TextStyle(
                                          fontSize: 15,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Padding(padding: EdgeInsets.only(left: 16)),
                                    Text(
                                      "\$" + calculateRate.subtotalPay,
                                      style: TextStyle(
                                          fontSize: 15, color: Colors.white),
                                    ),
                                  ],
                                  mainAxisAlignment: MainAxisAlignment.end,
                                ),
                                Padding(padding: EdgeInsets.only(bottom: 4)),
                                Row(
                                  children: <Widget>[
                                    Text(
                                      "Discount:",
                                      style: TextStyle(
                                          fontSize: 15,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Padding(padding: EdgeInsets.only(left: 16)),
                                    Text(
                                      "-\$" + calculateRate.discountPrice,
                                      style: TextStyle(
                                          fontSize: 15, color: Colors.white),
                                    ),
                                  ],
                                  mainAxisAlignment: MainAxisAlignment.end,
                                ),
                                Padding(padding: EdgeInsets.only(bottom: 4)),
                                Row(
                                  children: <Widget>[
                                    Text(
                                      "Shipping:",
                                      style: TextStyle(
                                          fontSize: 15,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Padding(padding: EdgeInsets.only(left: 16)),
                                    Text(
                                      "\$" + calculateRate.shippingPrice,
                                      style: TextStyle(
                                          fontSize: 15, color: Colors.white),
                                    ),
                                  ],
                                  mainAxisAlignment: MainAxisAlignment.end,
                                ),
                                Padding(padding: EdgeInsets.only(bottom: 4)),
                                Row(
                                  children: <Widget>[
                                    Text(
                                      "Taxes:",
                                      style: TextStyle(
                                          fontSize: 15,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Padding(padding: EdgeInsets.only(left: 16)),
                                    Text(
                                      "\$" + calculateRate.taxPrice,
                                      style: TextStyle(
                                          fontSize: 15, color: Colors.white),
                                    ),
                                  ],
                                  mainAxisAlignment: MainAxisAlignment.end,
                                ),
                                Container(
                                  height: 1,
                                  color: Colors.white,
                                  margin: EdgeInsets.only(top: 8, bottom: 16),
                                ),
                                Row(
                                  children: <Widget>[
                                    Text(
                                      "Total:",
                                      style: TextStyle(
                                          fontSize: 15,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Padding(padding: EdgeInsets.only(left: 16)),
                                    Text(
                                      "\$" + calculateRate.totalPay,
                                      style: TextStyle(
                                          fontSize: 15, color: Colors.white),
                                    ),
                                  ],
                                  mainAxisAlignment: MainAxisAlignment.end,
                                ),
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap: () {

                             Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => OrderTagPage2(orderID: calculateRate.orderID,totalAmount: calculateRate.totalPay,)),
                              );
                            },
                            child: Container(
                              width: 258,
                              height: 45,
                              margin: EdgeInsets.only(top: 64),
                              color: orangeColor,
                              child: Center(
                                child: Text(
                                  "Process Order",
                                  style: TextStyle(
                                      fontFamily: "Squada1",
                                      color: Colors.black,
                                      fontSize: 18),
                                ),
                              ),
                            ),
                          ),
                          Container(
                            width: 258,
                            height: 32,
                            margin: EdgeInsets.only(top: 16),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.only(bottom: 10, right: 4),
                                  width: 18,
                                  height: 18,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(16),
                                      color: Color(0xFF4F4F4F)),
                                ),
                                Container(
                                  margin: EdgeInsets.only(bottom: 10, right: 4),
                                  width: 18,
                                  height: 18,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(16),
                                    color: orangeColor,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(bottom: 10, right: 4),
                                  width: 18,
                                  height: 18,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(16),
                                    color: Color(0xFF4F4F4F),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(bottom: 10, right: 4),
                                  width: 18,
                                  height: 18,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(16),
                                    color: Color(0xFF4F4F4F),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                            width: 258,
                            height: 45,
                            margin: EdgeInsets.only(top: 16),
                            color: Color(0xFF4F4F4F),
                            child: Center(
                              child: Text(
                                "Exit",
                                style: TextStyle(
                                    fontFamily: "Squada1",
                                    color: Colors.white,
                                    fontSize: 18),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )),
    );
  }
/*
  Future<void> payStripe() async {
    try {
      await StripePayment.createTokenWithCard().then((token) async {});
    } catch (e) {
      Fluttertoast.showToast(msg: e);
    }
  }*/

/*
  Future<String> payOrder(String orderID, var token) async {
    String url = 'https://api.stripe.com/v1/orders/$orderID/pay';
    Map data = {
      'source': token.tokenId,
    };
    var response = await http.post(url,
        headers: {
          "Authorization": "Bearer " + "sk_test_AxUWnazrBOnBZDe07UYXdHSH",
        },
        body: data);
    print(response.body);
    return response.body;
  }
*/

/*
  Future<String> sendStatus() async {
    String url = 'https://pet-id.app/deploy/public/api/order-tag';
    Map data = {
      'pet_id': '2000',
      'total_price': 'usd',
      'discount_code': 'usd',
      'address1': val.addressLine1,
      'address2': val.addressLine2,
      'city': val.addressCity,
      'state': val.addressState,
      'zip_code': val.addressZip,
      'country_id':val.country,
    };
    var response = await http.post(url,
        headers: {
          "Authorization": "Bearer " + "sk_test_AxUWnazrBOnBZDe07UYXdHSH",
        },
        body: data);
    print(response.body);
    return response.body;
  }
*/

}
