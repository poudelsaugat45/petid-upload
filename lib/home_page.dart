import 'package:flutter/material.dart';
import 'package:pet_id/account_page.dart';
import 'package:pet_id/pet_info_page.dart';
import 'package:pet_id/recovery_page.dart';
import 'package:pet_id/scan_qr_page.dart';

class HomePage extends StatefulWidget {
  @override //new
  State<StatefulWidget> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<HomePage> {
  int _currentIndex = 0;
  var orangeColor = const Color(0xffF0F3BD);

  final List<Widget> _children = [
    RecoveryPage(),
    PetInfoPage(),
    ScanQrPage(),
    AccountPage(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: _children[_currentIndex],
      bottomNavigationBar: Theme(
        data: Theme.of(context).copyWith(
            canvasColor: const Color(0xff131313),
            primaryColor: const Color(0xffF0F3BD),
            textTheme: Theme.of(context)
                .textTheme
                .copyWith(caption: new TextStyle(color: Colors.white))),
        // sets the inactive color of the `BottomNavigationBar`

        child: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            onTap: onTabTapped,
            // new
            currentIndex: _currentIndex,
            items: [
              BottomNavigationBarItem(
                  icon: Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Image.asset(
                      "assets/recovery.png",
                      height: 28,
                      width: 28,
                      color: Colors.white,
                    ),
                  ),
                  activeIcon: Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Image.asset(
                      "assets/recovery.png",
                      height: 28,
                      width: 28,
                      color: orangeColor,
                    ),
                  ),
                  title: new Text("RECOVERY")),
              BottomNavigationBarItem(
                  icon: Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Image.asset(
                      "assets/pet.png",
                      height: 28,
                      width: 28,
                      color: Colors.white,
                    ),
                  ),
                  activeIcon: Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Image.asset(
                      "assets/pet.png",
                      height: 28,
                      width: 28,
                      color: orangeColor,
                    ),
                  ),
                  title: new Text("PET INFO")),
              BottomNavigationBarItem(
                  icon: Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Image.asset(
                      "assets/scan-qr.png",
                      height: 28,
                      width: 28,
                      color: Colors.white,
                    ),
                  ),
                  activeIcon: Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Image.asset(
                      "assets/scan-qr.png",
                      height: 28,
                      width: 28,
                      color: orangeColor,
                    ),
                  ),
                  title: new Text("SCAN QR")),
              BottomNavigationBarItem(
                  icon: Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Image.asset(
                      "assets/account.png",
                      height: 28,
                      width: 28,
                      color: Colors.white,
                    ),
                  ),
                  activeIcon: Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Image.asset(
                      "assets/account.png",
                      height: 28,
                      width: 28,
                      color: orangeColor,
                    ),
                  ),
                  title: new Text("ACCOUNT")),
            ]),
      ),
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}
