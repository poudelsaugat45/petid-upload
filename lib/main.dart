import 'package:flutter/material.dart';
import 'package:pet_id/forgot_password.dart';
import 'package:pet_id/home_page.dart';
import 'package:pet_id/login_page.dart';
import 'package:pet_id/onboarding_screen.dart';
import 'package:pet_id/privacy_policy_page.dart';
import 'package:pet_id/signup_page.dart';
import 'package:pet_id/start_page.dart';
import 'package:pet_id/terms_of_service_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          primarySwatch: Colors.blue,
          fontFamily: "Squada"
      ),
      initialRoute: '/',
      debugShowCheckedModeBanner: false,
      routes: {
        '/': (context) => StartPage(),
        '/login': (context) => LoginPage(),
        '/signup': (context) => SignupPage(),
        '/forgotpass': (context) => ForgotPasswordPage(),
        '/startpage1': (context) => StartPage(),
        '/startpage2': (context) => MyHomePage(),
        '/onboard': (context) => OnboardingPage(),
        '/mainhome': (context) => HomePage(),
        '/scanqr': (context) => MyHomePage(),
        '/privacy': (context) => PrivacyPage(),
        '/terms': (context) => TermsPage(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: GestureDetector(
          onTap: (){
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => StartPage()),
            );
          },
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            color: Colors.black,
          ),
        ),
      ),
    );
  }
}
