import 'dart:convert';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:pet_id/forgot_password.dart';
import 'package:pet_id/home_page.dart';
import 'package:pet_id/privacy_policy_page.dart';
import 'package:pet_id/signup_page.dart';
import 'package:pet_id/terms_of_service_page.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  double height;
  double width;
  bool _obscureText = true;
  var orangeColor = const Color(0xffF0F3BD);
  var textColor = const Color(0xffF2F2F2);
  ProgressDialog pr;

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  final myFirstNameController = TextEditingController();
  final myLastNameController = TextEditingController();
  final myPhoneController = TextEditingController();
  final myUserNameController = TextEditingController();
  final myPasswordController = TextEditingController();
  final myPasswordAgainController = TextEditingController();

  var subscribed = false;

  var firstName;
  var email;
  var lastName;

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;
    pr = new ProgressDialog(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(colors: [
              const Color(0xFF028090),
              const Color(0xFF05668D),
            ], begin: Alignment.bottomCenter, end: Alignment.centerRight),
          ),
          child: Column(
            children: <Widget>[
              Container(
                child: Column(
                  children: <Widget>[
                    Container(
                      height: height / 12,
                    ),
                    Container(
                      child: Column(
                        children: <Widget>[
                          Text(
                            "PETiD®",
                            style: TextStyle(fontFamily: "Squada1",fontSize: 70, color: orangeColor),
                          ),
                          Text(
                            "INTELLIGENT PET IDENTIFICATION",
                            style: TextStyle(
                                fontFamily: "Squada1",
                                fontSize: 12,
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                                letterSpacing: 0.1),
                          )
                        ],
                      ),
                    ),
                    Container(
                      width: 258,
                      margin: EdgeInsets.only(
                          top: height / 30, bottom: height / 16, right: 0),
                      child: Align(
                        alignment: AlignmentDirectional.centerStart,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Welcome Back, \nSign in to continue.",
                              style: TextStyle(
                                  color: textColor,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.left,
                            ),
                            Padding(padding: EdgeInsets.all(8)),
                            TextField(
                              controller: myUserNameController,
                              style: new TextStyle(color: Colors.white),
                              decoration: InputDecoration(
                                hintText: "Email",
                                hintStyle: TextStyle(
                                    color: textColor,
                                    fontWeight: FontWeight.bold),
                                enabledBorder: new UnderlineInputBorder(
                                    borderSide:
                                        new BorderSide(color: Colors.white)),
                              ),
                            ),
                            Padding(padding: EdgeInsets.all(2)),
                            TextField(
                              controller: myPasswordController,
                              style: new TextStyle(color: Colors.white),
                              obscureText: _obscureText,
                              decoration: InputDecoration(
                                  hintText: "Password",
                                  hintStyle: TextStyle(
                                      color: textColor,
                                      fontWeight: FontWeight.bold),
                                  enabledBorder: new UnderlineInputBorder(
                                      borderSide:
                                          new BorderSide(color: Colors.white)),
                                  suffixIcon: IconButton(
                                      icon: _obscureText
                                          ? Icon(
                                              Icons.visibility,
                                              color: Colors.white,
                                            )
                                          : Icon(
                                              Icons.visibility_off,
                                              color: Colors.white,
                                            ),
                                      onPressed: () {
                                        _toggle();
                                      })),
                            ),
                            Padding(padding: EdgeInsets.all(6)),
                            Align(
                                alignment: AlignmentDirectional.centerEnd,
                                child: GestureDetector(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              ForgotPasswordPage()),
                                    );
                                  },
                                  child: Text(
                                    "Forgot Password?",
                                    style: TextStyle(
                                        color: textColor,
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold),
                                    textAlign: TextAlign.center,
                                  ),
                                )),
                            Padding(padding: EdgeInsets.all(16)),
                            GestureDetector(
                              child: Container(
                                  width: 258,
                                  height: 45,
                                  color: orangeColor,
                                  child: Center(
                                      child: Text(
                                    "Login",
                                    style: TextStyle(
                                        fontFamily: "Squada1",
                                        fontSize: 17,
                                        fontWeight: FontWeight.w400),
                                  ))),
                              onTap: () {
                                pr.show();
                                tryLogin(myUserNameController.text,
                                        myPasswordController.text)
                                    .then((value) {
                                      pr.hide().then((va){
                                        decideLogin(value);
                                      });
                                });
                              },
                            ),
                            Padding(padding: EdgeInsets.all(8)),
                            /*InkWell(
                              onTap: (){

                                loginFacebook().then((val) {
                                  if (val != null) {
                                    final profile = json.decode(val);
                                    tryLoginFB(profile["id"]).then((value) {
                                      Navigator.pop(context);
                                      decideLogin(value);
                                    });
                                  }
                                });
                              },
                              child: Container(
                                  width: 258,
                                  height: 45,
                                  color: Colors.blue[900],
                                  child: Center(
                                      child: Text(
                                    "Login with Facebook",
                                    style: TextStyle(
                                        fontFamily: "Squada1",
                                        fontSize: 17,
                                        color: textColor,
                                        fontWeight: FontWeight.w400),
                                  ))),
                            ),*/
                            Padding(padding: EdgeInsets.all(8)),
                            Container(
                              alignment: AlignmentDirectional.center,
                              child: RichText(
                                textAlign: TextAlign.center,
                                text: TextSpan(
                                    text: 'New user? ',
                                    style: TextStyle(
                                        fontFamily: "Squada",
                                        fontSize: 15,
                                        color: textColor,
                                        fontWeight: FontWeight.bold),
                                    children: [
                                      TextSpan(
                                          text: "Signup",
                                          style: TextStyle(
                                              fontSize: 14, color: orangeColor),
                                          recognizer: TapGestureRecognizer()
                                            ..onTap = () {
                                              Navigator.pushReplacementNamed(
                                                context,"/signup",
                                              );
                                            }),
                                    ]),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 12),
                              child: RichText(
                                textAlign: TextAlign.center,
                                text: TextSpan(
                                    style: TextStyle(
                                      fontFamily: "Squada",
                                      fontSize: 13,
                                      color: textColor,
                                    ),
                                    text: "By using PET-ID you agree to our ",
                                    children: [
                                      TextSpan(
                                        recognizer: new TapGestureRecognizer()
                                          ..onTap = () {
                                            Navigator.pushReplacementNamed(
                                              context,"/terms"
                                            );
                                          },
                                        text: "Terms Of Service",
                                        style: TextStyle(
                                            decoration:
                                                TextDecoration.underline,
                                            color: orangeColor),
                                      ),
                                      TextSpan(
                                        text:
                                            ". Learn how we process your data by viewing our ",
                                      ),
                                      TextSpan(
                                          text: "Privacy Policy",
                                          style: TextStyle(
                                              decoration:
                                                  TextDecoration.underline,
                                              color: orangeColor),
                                          recognizer: new TapGestureRecognizer()
                                            ..onTap = () {
                                              Navigator.pushReplacementNamed(
                                                  context,"/privacy"
                                              );
                                            }),
                                    ]),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )),
    );
  }

  @override
  void dispose() {
    myUserNameController.dispose();
    myPasswordController.dispose();
    myPasswordAgainController.dispose();
    super.dispose();
  }


 /* Future<String> tryLoginFB(String provId) async {
    String url = 'https://pet-id.app/deploy/public/api/auth/login/fb';
    Map data = {'provider_id': provId};
    var bodyJson = json.encode(data);
    var response = await http.post(url,
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json"
        },
        body: bodyJson);
    return response.body;
  }*/

  Future<String> sendLogin(String userId) async {
    String url = 'https://pet-id.app/deploy/public/api/auth/login/fb';
    Map data = {
      'user_id': userId,
      //'logs': userSubscribed.toString(),
    };
    var bodyJson = json.encode(data);
    var response = await http.post(url,
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json"
        },
        body: bodyJson);
    return response.body;
  }

  _saveAccessToken(String accessToken) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString("access_token", accessToken);
  }

  Future<String> _checkLogin(BuildContext context) async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.get("access_token") != null) {
      return "logged_in";
    } else
      return "not_logged_in";
  }

 /*  Future<String> loginFacebook() async {
    final facebookLogin = FacebookLogin();
    facebookLogin.loginBehavior = FacebookLoginBehavior.webViewOnly;
    final result = await facebookLogin.logIn(['email']);

    if (result.status == FacebookLoginStatus.loggedIn) {
      final graphResponse = await http.get(
          'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=${result
              .accessToken.token}');
      return graphResponse.body;
    } else if (result.status == FacebookLoginStatus.cancelledByUser) {
      Fluttertoast.showToast(msg: "Cancelled User");
      return null;
    } else {
      Fluttertoast.showToast(msg: "Error");
      return null;
    }
  }*/


  decideLogin(String value) {
    var jsonResponse = json.decode(value);
    if (jsonResponse["error"] != null) {
      try{Fluttertoast.showToast(msg: jsonResponse["error"]);
      }catch(e){
      }
    } else {
      _saveAccessToken(jsonResponse["access_token"]);
      Navigator.pushReplacementNamed(context, '/mainhome');
    }
  }

  Future<String> tryLogin(String userName, String password) async {
    String url = 'https://pet-id.app/deploy/public/api/auth/login';
    print(userName+password);
    Map data = {'email': '$userName', 'password': '$password'};
    var bodyJson = json.encode(data);
    var response = await http.post(url,
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json"
        },
        body: bodyJson);
    print(response.body);
    return response.body;
  }
}
