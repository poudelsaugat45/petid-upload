import 'package:flutter/material.dart';
import 'package:pet_id/order_tag_page.dart';
import 'package:pet_id/order_tag_page_3.dart';

class ConfirmOrderPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => ConfirmOrderPageState();
}

class ConfirmOrderPageState extends State<ConfirmOrderPage> {
  double height;
  double width;
  bool _obscureText = true;
  var orangeColor = const Color(0xffF0F3BD);
  var textColor = const Color(0xffF2F2F2);

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  final myFirstNameController = TextEditingController();
  final myLastNameController = TextEditingController();
  final myPhoneController = TextEditingController();
  final myUserNameController = TextEditingController();
  final myPasswordController = TextEditingController();
  final myPasswordAgainController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  var subscribed = false;

  var firstName;
  var email;
  var lastName;

  @override
  void initState() {
    super.initState();
    asyncInitState();
  }

  void asyncInitState() async {}

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(colors: [
              const Color(0xFF028090),
              const Color(0xFF05668D),
            ], begin: Alignment.bottomCenter, end: Alignment.centerRight),
          ),
          child: Column(
            children: <Widget>[
              Container(
                child: Column(
                  children: <Widget>[
                    Container(
                      height: height / 12,
                    ),
                    Container(
                      child: Column(
                        children: <Widget>[
                          Text(
                            "PETiD®",
                            style: TextStyle(
                                fontFamily: "Squada1",
                                fontSize: 70, color: orangeColor),
                          ),
                          Text(
                            "INTELLIGENT PET IDENTIFICATION",
                            style: TextStyle(
                                fontFamily: "Squada1",
                                fontSize: 12,
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                                letterSpacing: 0.1),
                          )
                        ],
                      ),
                    ),
                    Container(
                      width: 258,
                      margin: EdgeInsets.only(
                          top: height / 30, bottom: height / 16, right: 0),
                      child: Column(
                        children: <Widget>[
                          Container(
                            width: 180,
                            child: Row(
                              children: <Widget>[
                                Image.asset(
                                  "assets/ptagsh.png",
                                  width: 84,
                                  height: 78,
                                ),
                                Container(
                                    padding: EdgeInsets.only(left: 16),
                                    width: 92,
                                    child: Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          "\$19.99",
                                          style: TextStyle(
                                              fontSize: 30, color: orangeColor),
                                        ),
                                        Text(
                                          "One time fee. Per tag , per pet.",
                                          style: TextStyle(
                                              fontSize: 10,
                                              color: Colors.white),
                                        )
                                      ],
                                    ))
                              ],
                            ),
                          ),
                          Text("Thank You!", style: TextStyle(color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                          Text("Your order has been placed. \nYou should receive a confirmation shorty.", style: TextStyle(color: Colors.white, fontSize: 15, fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                          Container(
                            margin: EdgeInsets.only(top: 64, left: 32, right: 32),
                            width: 258,
                            child: Text("*** Please allow up to two weeks for domestic deliveries and four to six weeks for international orders.", style: TextStyle(color: Colors.white, fontSize: 12,),),
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => OrderTagPage()),
                              );
                            },
                            child: Container(
                              width: 258,
                              height: 45,
                              margin: EdgeInsets.only(top: 64),
                              color: orangeColor,
                              child: Center(
                                child: Text(
                                  "Return to Profiles",
                                  style: TextStyle(
                                      fontFamily: "Squada1",
                                      color: Colors.black, fontSize: 18),
                                ),
                              ),
                            ),
                          ),
                          Container(
                            width: 258,
                            height: 32,
                            margin: EdgeInsets.only(top: 16),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.only(bottom: 10, right: 4),
                                  width: 18,
                                  height: 18,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(16),
                                      color: Color(0xFF4F4F4F)),
                                ),
                                Container(
                                  margin: EdgeInsets.only(bottom: 10, right: 4),
                                  width: 18,
                                  height: 18,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(16),
                                    color: Color(0xFF4F4F4F),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(bottom: 10, right: 4),
                                  width: 18,
                                  height: 18,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(16),
                                    color: Color(0xFF4F4F4F),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(bottom: 10, right: 4),
                                  width: 18,
                                  height: 18,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(16),
                                    color: orangeColor,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )),
    );
  }

  @override
  void dispose() {
    myUserNameController.dispose();
    myPasswordController.dispose();
    myPasswordAgainController.dispose();
    super.dispose();
  }
}
