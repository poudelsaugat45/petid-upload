import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:pet_id/privacy.dart';

class PrivacyPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var orangeColor = const Color(0xffF0F3BD);
    return Scaffold(
      appBar: PreferredSize(
          child: Container(
            padding: EdgeInsets.only(top: 32),
            color:  const Color(0xff131313),
            child: Column(
              children: <Widget>[
                Center(
                  child: Text(
                    "PETiD®",
                    style: TextStyle(
                        fontFamily: "Squada1",
                        color: orangeColor,
                        fontSize: 30,
                        letterSpacing: 1),
                    textAlign: TextAlign.justify,
                  ),
                ),
                Center(
                  child: Text(
                    "INTELLIGENT PET IDENTIFICATION",
                    style: TextStyle(
                        fontFamily: "Squada1",
                        color: const Color(0xffffffff),
                        fontSize: 7,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1),
                    textAlign: TextAlign.justify,
                  ),
                ),
              ],
            ),
          ),
          preferredSize: Size(360, 45)),
      body: Container(
        padding: EdgeInsets.only(top: 16),
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [
                const Color(0xFF05668D),
                const Color(0xFF05668D),
              ],
              begin: Alignment.bottomCenter,
              end: Alignment.centerRight),
        ),
        child: Column(
          children: <Widget>[
            Text("Privacy Policy", style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              color: Colors.white
            ),),
            Container(
              margin: EdgeInsets.only(top: 10),
              height: MediaQuery.of(context).size.height*0.64,
              width: 321,
              child: SingleChildScrollView(
                child: Html(
                  data: PrivacyString().privacy,
                  defaultTextStyle: TextStyle(color: Colors.white),
                ),
              ),
            ),
            Padding(padding: EdgeInsets.all(8)),
            InkWell(
              onTap: (){
                Navigator.pushReplacementNamed(context, "/login");
              },
              child: Container(
                height: 45,
                width: 321,
                decoration:
                BoxDecoration(
                    border: Border.all(color: orangeColor)
                ),
                child: Theme(
                  data: Theme.of(context).copyWith(
                    canvasColor: Colors.grey[900],
                  ),
                  child: Center(
                    child: Text(
                      "Login",
                      style: TextStyle(
                          fontFamily: "Squada1",
                          fontSize: 18,
                          color: Color(0xFFF2F2F2)),
                    ),
                  ),
                ),
              ),
            ),
            Padding(padding: EdgeInsets.all(8)),
            InkWell(
              onTap: (){
                Navigator.pushReplacementNamed(context, "/signup");
              },
              child: Container(
                height: 45,
                width: 321,
                decoration:
                BoxDecoration(
                  color: orangeColor
                ),
                child: Theme(
                  data: Theme.of(context).copyWith(
                    canvasColor: Colors.grey[900],
                  ),
                  child: Center(
                    child: Text("Signup", style: TextStyle(
                        fontFamily: "Squada1",fontSize: 18, color: Color(0xFF000000)),),
                  ),
                ),
              ),
            ),

          ],
        ),
      ),
    );
  }

}