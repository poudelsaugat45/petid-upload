class CalculateRate {
  final String totalPay;
  final String subtotalPay;
  final String taxPrice;
  final String discountPrice;
  final String shippingPrice;
  final String orderID;

  CalculateRate(this.totalPay, this.subtotalPay, this.taxPrice,
      this.discountPrice, this.shippingPrice, this.orderID);
}
