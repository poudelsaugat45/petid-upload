import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:pet_id/order_tag_page.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PetDetailsPage extends StatefulWidget {
  String petId;

  PetDetailsPage({this.petId});

  @override
  _PetDetailsPageState createState() => _PetDetailsPageState(petId: petId);
}

class _PetDetailsPageState extends State<PetDetailsPage> {
  var petList = [];
  String petId;

  _PetDetailsPageState({this.petId});

  final myNameController = TextEditingController();
  final colorController = TextEditingController();
  final breedController = TextEditingController();
  final notesController = TextEditingController();
  var imageOne;
  var imageTwo;
  var gender;
  var status = 0;
  File _image;
  var hasData = false;
  ProgressDialog pr;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPetDetails(petId).then((val) {
      updateValue(val);
    });
  }

  updateValue(val) {
    var jsonResponse;
    try {
      jsonResponse = json.decode(val);
      if (!jsonResponse['status']) {
        Fluttertoast.showToast(msg: jsonResponse['message']);
        return Container();
      } else {
        setState(() {
          hasData = true;
          myNameController.text = jsonResponse["data"]["name"];
          breedController.text = jsonResponse["data"]["breed"];
          colorController.text = jsonResponse["data"]["color"];
          notesController.text = jsonResponse["data"]["message"];
          imageOne = jsonResponse["data"]["image1"];
          imageTwo = jsonResponse["data"]["image2"];
          gender = jsonResponse["data"]["gender"];
          status = int.parse(jsonResponse["data"]["status"]);
        });
      }
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context);
    var orangeColor = const Color(0xffF0F3BD);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: PreferredSize(
          child: Container(
            padding: EdgeInsets.only(top: 32),
            color: const Color(0xff131313),
            child: Column(
              children: <Widget>[
                Center(
                  child: Text(
                    "PETiD®",
                    style: TextStyle(
                        fontFamily: "Squada1",
                        color: orangeColor, fontSize: 30, letterSpacing: 1),
                    textAlign: TextAlign.justify,
                  ),
                ),
                Center(
                  child: Text(
                    "INTELLIGENT PET IDENTIFICATION",
                    style: TextStyle(
                        fontFamily: "Squada1",
                        color: const Color(0xffffffff),
                        fontSize: 7,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1),
                    textAlign: TextAlign.justify,
                  ),
                ),
              ],
            ),
          ),
          preferredSize: Size(360, 45)),
      body: Container(
          padding: EdgeInsets.only(top: 16),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            gradient: LinearGradient(colors: [
              const Color(0xFF028090),
              const Color(0xFF05668D),
            ], begin: Alignment.bottomCenter, end: Alignment.centerRight),
          ),
          child: Center(
              child: hasData
                  ? Container(
                      width: 328,
                      child: Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              InkWell(
                                onTap: () {
                                  getImage().then((file) {
                                    if (file != null) {
                                      pr.show();
                                      getbaseImage(file).then((baseimage) {
                                        postImage("image1", baseimage, petId)
                                            .then((resp) {
                                          var jsonResp = json.decode(resp);
                                          pr.hide().then((v) {
                                            Fluttertoast.showToast(
                                                msg: jsonResp["message"]);
                                          });
                                          if (jsonResp["status"]) {
                                            setState(() {
                                              imageOne =
                                                  jsonResp["data"]["image1"];
                                            });
                                          }
                                        });
                                      });
                                    }
                                  });
                                },
                                child: Container(
                                  width: 149,
                                  height: 124,
                                  child: imageOne == null
                                      ? Image.asset("assets/take_photo.png",
                                          fit: BoxFit.fitHeight)
                                      : Image.network(
                                          imageOne,
                                          fit: BoxFit.fitHeight,
                                        ),
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  getImage().then((file) {
                                    if (file != null) {
                                      getbaseImage(file).then((baseimage) {
                                        pr.show();
                                        postImage("image2", baseimage, petId)
                                            .then((resp) {
                                          var jsonResp = json.decode(resp);
                                          pr.hide().then((v) {
                                            Fluttertoast.showToast(
                                                msg: jsonResp["message"]);
                                          });
                                          if (jsonResp["status"]) {
                                            setState(() {
                                              imageTwo =
                                                  jsonResp["data"]["image2"];
                                            });
                                          }
                                        });
                                      });
                                    }
                                  });
                                },
                                child: Container(
                                  width: 149,
                                  height: 124,
                                  child: imageTwo == null
                                      ? Image.asset("assets/take_photo.png",
                                          fit: BoxFit.fitHeight)
                                      : Image.network(
                                          imageTwo,
                                          fit: BoxFit.fitHeight,
                                        ),
                                ),
                              )
                            ],
                          ),
                          TextField(
                            maxLength: 12,
                            controller: myNameController,
                            style: new TextStyle(color: Colors.white),
                            decoration: InputDecoration(
                              suffixIcon: Icon(
                                Icons.check,
                                color: orangeColor,
                                size: 24,
                              ),
                              labelText: "Name:",
                              hintText: "Name",
                              alignLabelWithHint: true,
                              hintStyle: TextStyle(
                                  color: Color(0x44ffffff), fontSize: 15),
                              labelStyle: TextStyle(color: Colors.white),
                              enabledBorder: new UnderlineInputBorder(
                                  borderSide:
                                      new BorderSide(color: Colors.white)),
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(top: 24.0, bottom: 8),
                            child: Row(
                              children: <Widget>[
                                Text("Sex:",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold)),
                                Padding(
                                    padding: EdgeInsets.symmetric(
                                        vertical: 8, horizontal: 8)),
                                GestureDetector(
                                  child: Padding(
                                    padding: const EdgeInsets.only(right: 8.0),
                                    child: Text(
                                      "Male",
                                      style: TextStyle(
                                          color: gender == "Male"
                                              ? orangeColor
                                              : Colors.white,
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  onTap: () {
                                    setState(() {
                                      gender = "Male";
                                    });
                                  },
                                ),
                                GestureDetector(
                                  child: Padding(
                                    padding: const EdgeInsets.only(right: 8.0),
                                    child: Text(
                                      "Female",
                                      style: TextStyle(
                                          color: gender == "Female"
                                              ? orangeColor
                                              : Colors.white,
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  onTap: () {
                                    setState(() {
                                      gender = "Female";
                                    });
                                  },
                                ),
                              ],
                            ),
                          ),
                          TextField(
                            controller: colorController,
                            style: new TextStyle(color: Colors.white),
                            decoration: InputDecoration(
                              labelText: "Color: ",
                              suffixIcon: Icon(
                                Icons.check,
                                color: orangeColor,
                                size: 24,
                              ),
                              hintText: "Color",
                              hintStyle: TextStyle(
                                  color: Color(0x44ffffff), fontSize: 15),
                              labelStyle: TextStyle(color: Colors.white),
                              enabledBorder: new UnderlineInputBorder(
                                  borderSide:
                                      new BorderSide(color: Colors.white)),
                            ),
                          ),
                          TextField(
                            controller: breedController,
                            style: new TextStyle(color: Colors.white),
                            decoration: InputDecoration(
                              labelText: "Breed: ",
                              hintText: "Breed",
                              suffixIcon: Icon(
                                Icons.check,
                                color: orangeColor,
                                size: 24,
                              ),
                              hintStyle: TextStyle(
                                  color: Color(0x44ffffff), fontSize: 15),
                              labelStyle: TextStyle(color: Colors.white),
                              enabledBorder: new UnderlineInputBorder(
                                  borderSide:
                                      new BorderSide(color: Colors.white)),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 16.0),
                            child: Row(
                              children: <Widget>[
                                Text("Status:",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold)),
                                Padding(
                                    padding: EdgeInsets.symmetric(
                                        vertical: 8, horizontal: 8)),
                                GestureDetector(
                                  child: Padding(
                                    padding: const EdgeInsets.only(right: 8.0),
                                    child: Text(
                                      "Lost",
                                      style: TextStyle(
                                          color: status == 0
                                              ? Color(0xFF02C39A)
                                              : Colors.white,
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  onTap: () {
                                    setState(() {
                                      status = 0;
                                    });
                                  },
                                ),
                                GestureDetector(
                                  child: Padding(
                                    padding: const EdgeInsets.only(right: 8.0),
                                    child: Text(
                                      "Safe",
                                      style: TextStyle(
                                          color: status == 1
                                              ? orangeColor
                                              : Colors.white,
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  onTap: () {
                                    setState(() {
                                      status = 1;
                                    });
                                  },
                                ),
                              ],
                            ),
                          ),
                          Padding(padding: EdgeInsets.only(top: 8)),
                          Container(
                            color: Color(0x55333333),
                            child: TextField(
                              textInputAction: TextInputAction.done,
                              controller: notesController,
                              style: new TextStyle(color: Colors.white),
                              maxLines: 5,
                              minLines: 5,
                              decoration: InputDecoration(
                                isDense: false,
                                fillColor: Color(0x55333333),
                                labelText: "Notes: ",
                                hintText: "Notes",
                                hintMaxLines: 1,
                                alignLabelWithHint: true,
                                hintStyle: TextStyle(
                                    color: Color(0x44ffffff), fontSize: 15),
                                labelStyle: TextStyle(color: Colors.white),
                                enabledBorder: new OutlineInputBorder(
                                    borderSide: new BorderSide(
                                        color: Color(0xFFF0F3BD))),
                                focusedBorder: new OutlineInputBorder(
                                    borderSide: new BorderSide(
                                        color: Color(0xFFF0F3BD))),
                              ),
                            ),
                          ),
                          Padding(padding: EdgeInsets.only(top: 16)),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              InkWell(
                                onTap: () {
                                  pr.show();
                                  updatePetData().then((resp) {
                                    pr.hide().then((v){
                                      print(resp);
                                      try {
                                        var responseJson = json.decode(resp);

                                        if (responseJson["status"]) {
                                          Fluttertoast.showToast(
                                              msg: "Data updated succssfully");
                                          Navigator.pop(context);
                                        }else{
                                          Fluttertoast.showToast(
                                              msg: "Error occured while updating");
                                        }
                                      } catch (e) {}
                                    });
                                  });
                                },
                                child: Container(
                                  width: 150,
                                  height: 45,
                                  color: orangeColor,
                                  child: Center(
                                    child: Text(
                                      "Save",
                                      style: TextStyle(
                                          fontFamily: "Squada1",
                                          color: Colors.black, fontSize: 18),
                                    ),
                                  ),
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  pr.show();
                                  deletePetDetails(petId).then((value) {
                                    pr.hide().then((v){
                                      Navigator.pop(context);
                                    });
                                  });
                                },
                                child: Container(
                                  width: 150,
                                  height: 45,
                                  color: Color(0xFF333333),
                                  child: Center(
                                    child: Text(
                                      "Remove",
                                      style: TextStyle(
                                          fontFamily: "Squada1",
                                          color: Colors.white70, fontSize: 18),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        /*  GestureDetector(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => OrderTagPage(petID: petId,)),
                              );
                            },
                            child: Container(
                              margin: EdgeInsets.only(top: 16),
                              height: 45,
                              color: orangeColor,
                              child: Center(
                                child: Text(
                                  "Order Tag For ${myNameController.value.text}",
                                  style: TextStyle(
                                      fontFamily: "Squada1",
                                      color: Colors.black, fontSize: 18),
                                ),
                              ),
                            ),
                          ),*/
                        ],
                      ),
                    )
                  : CircularProgressIndicator())),
    );
  }

  Future<File> getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image = image;
    });
    return image;
  }

  Future<String> updatePetData() async {
    final prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString("access_token");
    Map data = {
      'name': myNameController.text,
      'gender': gender,
      'color': colorController.text,
      'breed': breedController.text,
      'message': notesController.text,
      'status': status,
    };
    print(data);
    var bodyJson = json.encode(data);
    String url = 'https://pet-id.app/deploy/public/api/my-pet/$petId';
    var response = await http.post(url,
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
          "Authorization": "Bearer " + accessToken,
        },
        body: bodyJson);
    return response.body;
  }
}

Future<String> getPetDetails(String petId) async {
  final prefs = await SharedPreferences.getInstance();
  String accessToken = prefs.getString("access_token");

  String url = 'https://pet-id.app/deploy/public/api/my-pet/$petId';
  var response = await http.get(
    url,
    headers: {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer " + accessToken,
    },
  );
  print(response.body + petId);
  return response.body;
}

Future<String> deletePetDetails(String petId) async {
  final prefs = await SharedPreferences.getInstance();
  String accessToken = prefs.getString("access_token");

  String url = 'https://pet-id.app/deploy/public/api/my-pet/$petId';
  var response = await http.delete(
    url,
    headers: {
      "Authorization": "Bearer " + accessToken,
    },
  );
  print(response.body + petId);
  return response.body;
}

Future<String> postImage(
    String imageId, String base64Image, String petId) async {
  final prefs = await SharedPreferences.getInstance();
  String accessToken = prefs.getString("access_token");

  String url = 'https://pet-id.app/deploy/public/api/my-pet-image/$petId';
  Map data = {
    "image_key": imageId,
    "image": "data:image/jpeg;base64," + base64Image
  };
  var bodyJson = json.encode(data);
  var response = await http.post(url,
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": "Bearer " + accessToken,
      },
      body: bodyJson);
  print(response.body + petId);
  return response.body;
}

Future<String> getbaseImage(File image) async {
  List<int> imageBytes = await image.readAsBytesSync();
  String base64Image = base64Encode(imageBytes);
  return base64Image;
}
