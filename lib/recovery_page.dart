import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RecoveryPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _RecoveryPageState();
}

class _RecoveryPageState extends State<RecoveryPage> {
  bool yesReward = false;
  var orangeColor = const Color(0xffF0F3BD);
  TextEditingController myNameController;
  TextEditingController myEmailController;
  TextEditingController myPhoneController;
  TextEditingController myPhoneController2;
  TextEditingController myAddressController;
  TextEditingController myAddressController2;
  TextEditingController myCityController;
  TextEditingController myStateController;
  TextEditingController myZipController;
  TextEditingController myCountryController;
  ProgressDialog pr;
  var futureVar = false;
  var phone1data;
  var phone2data;

  @override
  void initState() {
    super.initState();
    myNameController = TextEditingController();
    myEmailController = TextEditingController();
    myPhoneController = TextEditingController();
    myPhoneController2 = TextEditingController();
    myAddressController = TextEditingController();
    myAddressController2 = TextEditingController();
    myCityController = TextEditingController();
    myStateController = TextEditingController();
    myZipController = TextEditingController();
    myCountryController = TextEditingController();
    updateData();
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    myNameController.dispose();
    myEmailController.dispose();
    myPhoneController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context);
    return Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        backgroundColor: const Color(0xFF05668D),
        appBar: PreferredSize(
            child: Container(
              padding: EdgeInsets.only(top: 32),
              color: const Color(0xff131313),
              child: Column(
                children: <Widget>[
                  Center(
                    child: Text(
                      "PETiD®",
                      style: TextStyle(
                          fontFamily: "Squada1",
                          color: orangeColor, fontSize: 30, letterSpacing: 1),
                      textAlign: TextAlign.justify,
                    ),
                  ),
                  Center(
                    child: Text(
                      "INTELLIGENT PET IDENTIFICATION",
                      style: TextStyle(
                          fontFamily: "Squada1",
                          color: const Color(0xffffffff),
                          fontSize: 7,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 1),
                      textAlign: TextAlign.justify,
                    ),
                  ),
                ],
              ),
            ),
            preferredSize: Size(360, 45)),
        body: futureVar
            ? Container(
                padding: EdgeInsets.only(top: 16),
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  gradient: LinearGradient(colors: [
                    const Color(0xFF028090),
                    const Color(0xFF05668D),
                  ], begin: Alignment.bottomCenter, end: Alignment.centerRight),
                ),
                child: Center(
                    child: Container(
                  padding: EdgeInsets.only(top: 8),
                  width: 333,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Pet Recovery Info:",
                        style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                            color: orangeColor),
                      ),
                      TextField(
                        style: new TextStyle(color: Colors.white),
                        controller: myNameController,
                        decoration: InputDecoration(
                          hintText: "Name",
                          hintStyle: TextStyle(color: Colors.white),
                          enabledBorder: new UnderlineInputBorder(
                              borderSide: new BorderSide(color: Colors.white)),
                        ),
                      ),
                      TextField(
                        style: new TextStyle(color: Colors.white),
                        controller: myEmailController,
                        decoration: InputDecoration(
                          hintText: "Email",
                          hintStyle: TextStyle(color: Colors.white),
                          enabledBorder: new UnderlineInputBorder(
                              borderSide: new BorderSide(color: Colors.white)),
                        ),
                      ),
                      TextField(
                        style: new TextStyle(color: Colors.white),
                        controller: myPhoneController,
                        decoration: InputDecoration(
                          hintText: "Phone 1",
                          hintStyle: TextStyle(color: Colors.white),
                          enabledBorder: new UnderlineInputBorder(
                              borderSide: new BorderSide(color: Colors.white)),
                        ),
                      ),
                      TextField(
                        style: new TextStyle(color: Colors.white),
                        controller: myPhoneController2,
                        decoration: InputDecoration(
                          hintText: "Phone 2",
                          hintStyle: TextStyle(color: Colors.white),
                          enabledBorder: new UnderlineInputBorder(
                              borderSide: new BorderSide(color: Colors.white)),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 16.0),
                        child: Row(
                          children: <Widget>[
                            Text("Reward",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold)),
                            Padding(
                                padding: EdgeInsets.symmetric(
                                    vertical: 8, horizontal: 8)),
                            GestureDetector(
                              child: Padding(
                                padding: const EdgeInsets.only(right: 8.0),
                                child: Text(
                                  "Yes",
                                  style: TextStyle(
                                      color: yesReward?orangeColor:Colors.white,
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              onTap: () {
                                setState(() {
                                  yesReward = true;
                                });
                              },
                            ),
                            GestureDetector(
                              child: Padding(
                                padding: const EdgeInsets.only(right: 8.0),
                                child: Text(
                                  "No",
                                  style: TextStyle(
                                      color: yesReward?Colors.white:orangeColor,
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              onTap: () {
                                setState(() {
                                  yesReward = false;
                                });
                              },
                            ),
                          ],
                        ),
                      ),
                      TextField(
                        controller: myAddressController,
                        style: new TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                          hintText: "Address 1",
                          hintStyle: TextStyle(color: Colors.white),
                          enabledBorder: new UnderlineInputBorder(
                              borderSide: new BorderSide(color: Colors.white)),
                        ),
                      ),
                      TextField(
                        controller: myAddressController2,
                        style: new TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                          hintText: "Address 2",
                          hintStyle: TextStyle(color: Colors.white),
                          enabledBorder: new UnderlineInputBorder(
                              borderSide: new BorderSide(color: Colors.white)),
                        ),
                      ),
                      TextField(
                        controller: myCityController,
                        style: new TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                          hintText: "City",
                          hintStyle: TextStyle(color: Colors.white),
                          enabledBorder: new UnderlineInputBorder(
                              borderSide: new BorderSide(color: Colors.white)),
                        ),
                      ),
                      TextField(
                        controller: myStateController,
                        style: new TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                          hintText: "State",
                          hintStyle: TextStyle(color: Colors.white),
                          enabledBorder: new UnderlineInputBorder(
                              borderSide: new BorderSide(color: Colors.white)),
                        ),
                      ),
                      TextField(
                        controller: myZipController,
                        style: new TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                          hintText: "ZIP",
                          hintStyle: TextStyle(color: Colors.white),
                          enabledBorder: new UnderlineInputBorder(
                              borderSide: new BorderSide(color: Colors.white)),
                        ),
                      ),
                      TextField(
                        controller: myCountryController,
                        style: new TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                          hintText: "Country",
                          hintStyle: TextStyle(color: Colors.white),
                          enabledBorder: new UnderlineInputBorder(
                              borderSide: new BorderSide(color: Colors.white)),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          pr.style(
                            message: "Updating data, Please wait.."
                          );
                          pr.show();
                          updateRecoveryData().then((val) {
                            pr.hide().then((v){
                              var responseValue = jsonDecode(val);
                              Fluttertoast.showToast(msg: responseValue["message"]);
                            });
                          });
                        },
                        child: Container(
                          width: 333,
                          height: 45,
                          margin: EdgeInsets.symmetric(vertical: 32),
                          child: Center(
                              child: Text(
                            "Save",
                            style: TextStyle(
                                fontFamily: "Squada1",
                                fontWeight: FontWeight.w500, fontSize: 18),
                          )),
                          color: orangeColor,
                        ),
                      )
                    ],
                  ),
                )))
            : Center(
                child: CircularProgressIndicator(),
              ));
  }

  updateData() {
    getRecoveryData().then((value) {
      var jsonResponse;
      try {
        jsonResponse = json.decode(value);
        if (jsonResponse['message'] != null &&
            jsonResponse['message'] == "Token has expired") {
          if (jsonResponse['message'] == "Token has expired") {
            //logout(context);
          }
          Fluttertoast.showToast(msg: jsonResponse['message']);
          logout(context);
        }
        myNameController.text = jsonResponse['name'];
        myEmailController.text = jsonResponse['email'];
        myPhoneController.text = jsonResponse['phone1'];
        phone1data = jsonResponse['phone1'];
        myPhoneController2.text = jsonResponse['phone2'];
        phone2data = jsonResponse['phone2'];
        myAddressController.text = jsonResponse['address1'];
        myAddressController2.text = jsonResponse['address2'];
        myCityController.text = jsonResponse['city'];
        myZipController.text = jsonResponse['zip'];
        myStateController.text = jsonResponse['state'];
        myCountryController.text = jsonResponse['country'];


        setState(() {
          futureVar = true;
          if (jsonResponse['reward'] == "1") {
            yesReward = true;
          } else {
            yesReward = false;
          }
        });
      } catch (e) {}
    });
  }
  Future<String> updateRecoveryData() async {
    final prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString("access_token");
    Map data = {
      'name': myNameController.text,
      'email': myEmailController.text,
      'phone1': myPhoneController.text,
      'phone2': myPhoneController2.text,
      'address1': myAddressController.text,
      'address2': myAddressController2.text,
      'address2': myAddressController2.text,
      'city': myCityController.text,
      'zip': myZipController.text,
      'state': myStateController.text,
      'country': myCountryController.text,
      'reward': yesReward,
    };
    var bodyJson = json.encode(data);
    String url = 'https://pet-id.app/deploy/public/api/contact-info';
    var response = await http.post(url,
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
          "Authorization": "Bearer " + accessToken,
        },
        body: bodyJson);
    return response.body;
  }
}

void logout(BuildContext context) async {
  final prefs = await SharedPreferences.getInstance();
  prefs.remove("access_token");
  Navigator.pushReplacementNamed(context, "/login");
}

Future<String> getRecoveryData() async {
  final prefs = await SharedPreferences.getInstance();
  String accessToken = prefs.getString("access_token");

  String url = 'https://pet-id.app/deploy/public/api/contact-info';
  var response = await http.get(
    url,
    headers: {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer " + accessToken,
    },
  );
  return response.body;
}
